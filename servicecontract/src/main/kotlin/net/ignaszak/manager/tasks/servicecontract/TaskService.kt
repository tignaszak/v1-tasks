package net.ignaszak.manager.tasks.servicecontract

import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO
import net.ignaszak.manager.tasks.to.task.TaskFilterTO
import net.ignaszak.manager.tasks.to.task.TaskTO

interface TaskService {
    fun getById(publicId: String, userPublicId: String): TaskTO
    fun create(createUpdateTaskTO: CreateUpdateTaskTO, userPublicId: String): TaskTO
    fun create(createUpdateTaskTO: CreateUpdateTaskTO, projectPublicId: String, userPublicId: String): TaskTO
    fun update(createUpdateTaskTO: CreateUpdateTaskTO, taskPublicId: String, userPublicId: String): TaskTO
    fun deleteById(publicId: String, userPublicId: String)

    fun getTasks(criteriaTO: CriteriaTO<TaskFilterTO>): Set<TaskTO>
    fun getTasksCount(criteriaTO: CriteriaTO<TaskFilterTO>): Long
}