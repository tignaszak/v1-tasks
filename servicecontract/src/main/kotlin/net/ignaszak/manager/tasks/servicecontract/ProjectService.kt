package net.ignaszak.manager.tasks.servicecontract

import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import net.ignaszak.manager.tasks.to.project.ProjectTO

interface ProjectService {
    fun getById(publicId: String): ProjectTO
    fun create(createUpdateProjectTO: CreateUpdateProjectTO, userPublicId: String): ProjectTO
    fun update(createUpdateProjectTO: CreateUpdateProjectTO, projectPublicId: String): ProjectTO
    fun deleteById(publicId: String)
    fun attachTask(projectPublicId: String, taskPublicId: String, userPublicId: String)
    fun detachTask(projectPublicId: String, taskPublicId: String, userPublicId: String)

    fun getProjects(criteriaTO: CriteriaTO<ProjectFilterTO>): Set<ProjectTO>
    fun getProjectsCount(criteriaTO: CriteriaTO<ProjectFilterTO>): Long
}