package net.ignaszak.manager.tasks.servicecontract

import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.to.project.member.MemberFilterTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO

interface MemberService {
    fun getMembers(projectPublicId: String, userPublicId: String, criteriaTO: CriteriaTO<MemberFilterTO>): Set<MemberTO>
    fun getMembersCount(projectPublicId: String, criteriaTO: CriteriaTO<MemberFilterTO>): Long

    fun addMember(memberTO: MemberTO, projectPublicId: String, userPublicId: String)
    fun updateMember(memberTO: MemberTO, projectPublicId: String, userPublicId: String)
    fun deleteMember(memberPublicId: String, projectPublicId: String, userPublicId: String)
    fun joinProject(projectPublicId: String, userPublicId: String)
    fun leaveProject(projectPublicId: String, userPublicId: String)
}