package net.ignaszak.manager.tasks.service.project.member.mapper

import net.ignaszak.manager.tasks.dto.project.member.MemberDTO
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import net.ignaszak.manager.tasks.to.project.member.MemberTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.stereotype.Component

@Component
@Mapper(componentModel = "spring")
interface MemberMapper {

    @Mapping(source = "memberDTO.role", target = "role")
    fun memberDTOToMemberTO(memberDTO: MemberDTO): MemberTO
    fun memberTORoleToMemberEntityRole(role: MemberTO.Role): ProjectMemberEntity.UserRole
}