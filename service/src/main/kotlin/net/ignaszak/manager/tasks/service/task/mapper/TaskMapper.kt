package net.ignaszak.manager.tasks.service.task.mapper

import net.ignaszak.manager.tasks.dto.task.TaskDTO
import net.ignaszak.manager.tasks.entity.TaskEntity
import net.ignaszak.manager.tasks.to.task.TaskTO
import org.mapstruct.Mapper
import org.springframework.stereotype.Component

@Component
@Mapper(componentModel = "spring")
interface TaskMapper {
    fun taskEntityToTaskTO(taskEntity: TaskEntity): TaskTO
    fun taskDTOToTaskTO(taskDTO: TaskDTO): TaskTO
}