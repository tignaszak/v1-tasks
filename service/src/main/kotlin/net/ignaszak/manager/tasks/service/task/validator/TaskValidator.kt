package net.ignaszak.manager.tasks.service.task.validator

import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO


interface TaskValidator {

    fun valid(task: CreateUpdateTaskTO)

    fun validAuthorPublicId(authorPublicId: String, userPublicId: String)
}