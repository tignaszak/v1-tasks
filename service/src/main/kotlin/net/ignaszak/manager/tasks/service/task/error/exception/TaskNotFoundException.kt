package net.ignaszak.manager.tasks.service.task.error.exception

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.tasks.service.task.error.TaskError
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class TaskNotFoundException: ErrorException(TaskError.TSK_NOT_FOUND)