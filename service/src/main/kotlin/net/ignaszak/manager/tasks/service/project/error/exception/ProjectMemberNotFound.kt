package net.ignaszak.manager.tasks.service.project.error.exception

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_NOT_FOUND
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(NOT_FOUND)
class ProjectMemberNotFound: ErrorException(PRO_MEMBER_NOT_FOUND)