package net.ignaszak.manager.tasks.service.project.validator

import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO

interface ProjectValidator {
    fun valid(project: CreateUpdateProjectTO)
    fun validPublicId(publicId: String)
    fun validMemberPublicId(publicId: String)
}