package net.ignaszak.manager.tasks.service.project.error

import net.ignaszak.manager.commons.error.Error

enum class ProjectError(override val code: String, override val message: String) : Error {
    PRO_INVALID_TITLE("PRO-1", "Invalid project title!"),
    PRO_NOT_FOUND("PRO-2", "Project not found!"),
    PRO_INVALID_PUBLIC_ID("PRO-3", "Invalid project public id!"),
    PRO_INVALID_MEMBER_PUBLIC_ID("PRO-4", "Invalid project member public id!"),
    PRO_MEMBER_FORBIDDEN("PRO-5", "Invalid project member!"),
    PRO_INVALID_MEMBER_ROLE("PRO-6", "Invalid project member role!"),
    PRO_MEMBER_NOT_FOUND("PRO-7", "Project member not found!"),
    PRO_MEMBER_EXISTS("PRO-8", "Member already added to project!")
}