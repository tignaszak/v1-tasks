package net.ignaszak.manager.tasks.service.task.validator.impl

import net.ignaszak.manager.commons.error.ErrorCollector
import net.ignaszak.manager.commons.error.ErrorCollectorImpl
import net.ignaszak.manager.tasks.service.task.error.TaskError
import net.ignaszak.manager.tasks.service.task.validator.TaskValidator
import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO
import org.springframework.stereotype.Component

@Component
class TaskValidatorImpl : TaskValidator {

    private val errorCollector: ErrorCollector

    init {
        errorCollector = ErrorCollectorImpl()
    }

    override fun valid(task: CreateUpdateTaskTO) {
        if (task.title.isEmpty()) {
            errorCollector.add(TaskError.TSK_INVALID_TITLE)
        }
        errorCollector.throwException()
    }

    override fun validAuthorPublicId(authorPublicId: String, userPublicId: String) {
        if (authorPublicId != userPublicId) {
            errorCollector.add(TaskError.TSK_ACCESS_FORBIDDEN)
        }
        errorCollector.throwException()
    }
}