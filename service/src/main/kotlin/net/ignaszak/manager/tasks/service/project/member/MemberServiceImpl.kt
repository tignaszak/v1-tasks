package net.ignaszak.manager.tasks.service.project.member

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity.UserRole.ADMINISTRATOR
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity.UserRole.MEMBER
import net.ignaszak.manager.tasks.repository.project.ProjectRepository
import net.ignaszak.manager.tasks.repository.project.member.MemberRepository
import net.ignaszak.manager.tasks.service.project.error.ProjectError.*
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberForbiddenException
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberNotFound
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectNotFoundException
import net.ignaszak.manager.tasks.service.project.member.mapper.MemberMapper
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import net.ignaszak.manager.tasks.service.project.validator.ProjectValidator
import net.ignaszak.manager.tasks.servicecontract.MemberService
import net.ignaszak.manager.tasks.to.project.member.MemberFilterTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.stream.Collectors

@Service
open class MemberServiceImpl(private val memberRepository: MemberRepository,
                             private val projectRepository: ProjectRepository,
                             private val projectValidator: ProjectValidator,
                             private val memberMapper: MemberMapper,
                             private val memberValidator: MemberValidator,
                             private val criteriaService: CriteriaService) : MemberService {

    override fun getMembers(projectPublicId: String, userPublicId: String, criteriaTO: CriteriaTO<MemberFilterTO>): Set<MemberTO> {
        projectValidator.validPublicId(projectPublicId)
        projectValidator.validMemberPublicId(userPublicId)

        val projectEntity = projectRepository.findProjectEntityByPublicId(projectPublicId)
            .orElseThrow { ProjectNotFoundException() }

        if (!memberRepository.existsProjectMemberEntityByUserPublicIdAndProject(userPublicId, projectEntity)) {
            throw ProjectMemberForbiddenException()
        }

        val criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO)

        return memberRepository.findMemberDTOSetByProjectPublicId(projectPublicId, criteriaQuery)
            .stream()
            .map(memberMapper::memberDTOToMemberTO)
            .collect(Collectors.toCollection{
                LinkedHashSet()
            })
    }

    @Transactional(readOnly = true)
    override fun getMembersCount(projectPublicId: String, criteriaTO: CriteriaTO<MemberFilterTO>): Long {
        val criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO)
        return memberRepository.countMemberDTOSetByProjectPublicId(projectPublicId, criteriaQuery)
    }

    @Transactional
    override fun addMember(memberTO: MemberTO, projectPublicId: String, userPublicId: String) {
        validData(projectPublicId, userPublicId, memberTO.publicId)

        val projectEntity = getProjectEntity(projectPublicId)

        validAdministrator(userPublicId, projectEntity.publicId)

        val memberEntity = when (memberTO.role) {
            Role.ADMINISTRATOR -> ADMINISTRATOR.of(memberTO.publicId)
            Role.MEMBER        -> MEMBER.of(memberTO.publicId)
            else               -> throw ValidationException(PRO_INVALID_MEMBER_ROLE)
        }

        memberEntity.addProject(projectEntity)

        memberRepository.save(memberEntity)
    }

    @Transactional
    override fun updateMember(memberTO: MemberTO, projectPublicId: String, userPublicId: String) {
        validData(projectPublicId, userPublicId, memberTO.publicId)

        val projectEntity = getProjectEntity(projectPublicId)

        validAdministrator(userPublicId, projectEntity.publicId)

        val memberEntity = getMemberEntity(memberTO.publicId, projectEntity)

        val role = memberMapper.memberTORoleToMemberEntityRole(memberTO.role)

        memberEntity.userRole = role
    }

    @Transactional
    override fun deleteMember(memberPublicId: String, projectPublicId: String, userPublicId: String) {
        validData(projectPublicId, userPublicId, memberPublicId)

        val projectEntity = getProjectEntity(projectPublicId)

        validAdministrator(userPublicId, projectEntity.publicId)

        val memberEntity = getMemberEntity(memberPublicId, projectEntity)

        memberRepository.delete(memberEntity)
    }

    @Transactional
    override fun joinProject(projectPublicId: String, userPublicId: String) {
        projectValidator.validPublicId(projectPublicId)
        projectValidator.validMemberPublicId(userPublicId)

        val projectEntity = getProjectEntity(projectPublicId)

        memberRepository.findProjectMemberEntityByUserPublicIdAndProject(userPublicId, projectEntity)
            .ifPresent { throw ValidationException(PRO_MEMBER_EXISTS) }

        val memberEntity = MEMBER.of(userPublicId)
        memberEntity.addProject(projectEntity)

        memberRepository.save(memberEntity)
    }

    @Transactional
    override fun leaveProject(projectPublicId: String, userPublicId: String) {
        projectValidator.validPublicId(projectPublicId)
        projectValidator.validMemberPublicId(userPublicId)

        val projectEntity = getProjectEntity(projectPublicId)

        val memberEntity = getMemberEntity(userPublicId, projectEntity)

        memberRepository.delete(memberEntity)
    }

    private fun getMemberEntity(memberPublicId: String, projectEntity: ProjectEntity) : ProjectMemberEntity {
        val entity = memberRepository.findProjectMemberEntityByUserPublicIdAndProject(memberPublicId, projectEntity)
            .orElseThrow { ProjectMemberNotFound() }

        if (entity.isProjectCreator) throw ProjectMemberForbiddenException()

        return entity
    }

    private fun validData(projectPublicId: String, userPublicId: String, memberPublicId: String) {
        projectValidator.validPublicId(projectPublicId)
        projectValidator.validMemberPublicId(userPublicId)
        projectValidator.validMemberPublicId(memberPublicId)
    }

    private fun getProjectEntity(projectPublicId: String) =
        projectRepository.findProjectEntityByPublicId(projectPublicId)
            .orElseThrow { ProjectNotFoundException() }

    private fun validAdministrator(userPublicId: String, projectPublicId: String) {
        memberValidator.valid(
            MemberCriteria
                .user(userPublicId)
                .joinedProject(projectPublicId)
                .withRoleEq(Role.ADMINISTRATOR)
        )
    }
}