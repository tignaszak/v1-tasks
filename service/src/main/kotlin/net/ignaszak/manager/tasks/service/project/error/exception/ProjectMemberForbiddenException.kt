package net.ignaszak.manager.tasks.service.project.error.exception

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_FORBIDDEN
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(FORBIDDEN)
class ProjectMemberForbiddenException: ErrorException(PRO_MEMBER_FORBIDDEN)