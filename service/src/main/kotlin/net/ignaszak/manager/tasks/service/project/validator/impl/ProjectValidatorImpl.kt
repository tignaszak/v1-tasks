package net.ignaszak.manager.tasks.service.project.validator.impl

import net.ignaszak.manager.commons.error.ErrorCollector
import net.ignaszak.manager.commons.error.ErrorCollectorImpl
import net.ignaszak.manager.tasks.service.project.error.ProjectError
import net.ignaszak.manager.tasks.service.project.validator.ProjectValidator
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import org.springframework.stereotype.Component
import java.lang.Exception
import java.util.UUID

@Component
class ProjectValidatorImpl : ProjectValidator {

    private val errorCollector: ErrorCollector

    init {
        errorCollector = ErrorCollectorImpl()
    }

    override fun valid(project: CreateUpdateProjectTO) {
        if (project.title.isBlank()) {
            errorCollector.add(ProjectError.PRO_INVALID_TITLE)
        }
        errorCollector.throwException()
    }

    override fun validPublicId(publicId: String) {
        validPublicId(publicId, ProjectError.PRO_INVALID_PUBLIC_ID)
    }

    override fun validMemberPublicId(publicId: String) {
        validPublicId(publicId, ProjectError.PRO_INVALID_MEMBER_PUBLIC_ID)
    }

    private fun validPublicId(publicId: String, error: net.ignaszak.manager.commons.error.Error) {
        try {
            UUID.fromString(publicId)
        } catch (e: Exception) {
            errorCollector.add(error)
            errorCollector.throwException()
        }
    }
}