package net.ignaszak.manager.tasks.service.task

import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tasks.entity.TaskEntity
import net.ignaszak.manager.tasks.repository.project.ProjectRepository
import net.ignaszak.manager.tasks.to.task.TaskTO
import net.ignaszak.manager.tasks.repository.task.TaskRepository
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectNotFoundException
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import net.ignaszak.manager.tasks.service.task.error.exception.TaskNotFoundException
import net.ignaszak.manager.tasks.service.task.mapper.TaskMapper
import net.ignaszak.manager.tasks.service.task.validator.TaskValidator
import net.ignaszak.manager.tasks.servicecontract.TaskService
import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO
import net.ignaszak.manager.tasks.to.task.TaskFilterTO
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.stream.Collectors

@Service
open class TaskServiceImpl(
    private val memberValidator: MemberValidator,
    private val taskRepository: TaskRepository,
    private val projectRepository: ProjectRepository,
    private val taskValidator: TaskValidator,
    private val taskMapper: TaskMapper,
    private val criteriaService: CriteriaService,
    private val dateProvider: IDateTimeProvider
): TaskService {

    @Transactional(readOnly = true)
    override fun getById(publicId: String, userPublicId: String): TaskTO {
        val taskEntity = taskRepository.findTaskEntityByPublicId(publicId).orElseThrow{ TaskNotFoundException() }
        taskValidator.validAuthorPublicId(taskEntity.authorPublicId, userPublicId)
        return taskMapper.taskEntityToTaskTO(taskEntity)
    }

    @Transactional
    override fun create(createUpdateTaskTO: CreateUpdateTaskTO, userPublicId: String): TaskTO {
        taskValidator.valid(createUpdateTaskTO)
        val taskEntity = TaskEntity(userPublicId, dateProvider.getNow())
        fillTaskEntity(taskEntity, createUpdateTaskTO)
        taskRepository.save(taskEntity)
        return taskMapper.taskEntityToTaskTO(taskEntity)
    }

    @Transactional
    override fun create(createUpdateTaskTO: CreateUpdateTaskTO, projectPublicId: String, userPublicId: String): TaskTO {
        taskValidator.valid(createUpdateTaskTO)

        val projectEntity = projectRepository.findProjectEntityByPublicId(projectPublicId)
            .orElseThrow{ProjectNotFoundException()}

        memberValidator.valid(
            MemberCriteria
                .user(userPublicId)
                .joinedProject(projectPublicId)
                .withAnyRole()
        )

        val taskEntity = TaskEntity(userPublicId, dateProvider.getNow())
        taskEntity.addProject(projectEntity)
        fillTaskEntity(taskEntity, createUpdateTaskTO)
        taskRepository.save(taskEntity)

        return taskMapper.taskEntityToTaskTO(taskEntity)
    }

    @Transactional
    override fun update(createUpdateTaskTO: CreateUpdateTaskTO, taskPublicId: String, userPublicId: String): TaskTO {
        taskValidator.valid(createUpdateTaskTO)
        val taskEntity = taskRepository.findTaskEntityByPublicId(taskPublicId).orElseThrow{ TaskNotFoundException() }
        taskValidator.validAuthorPublicId(taskEntity.authorPublicId, userPublicId)
        fillTaskEntity(taskEntity, createUpdateTaskTO)
        taskEntity.modificationDate = dateProvider.getNow()
        return taskMapper.taskEntityToTaskTO(taskEntity)
    }

    @Transactional
    override fun deleteById(publicId: String, userPublicId: String) {
        val taskEntity = taskRepository.findTaskEntityByPublicId(publicId).orElseThrow{ TaskNotFoundException() }
        taskValidator.validAuthorPublicId(taskEntity.authorPublicId, userPublicId)
        taskRepository.delete(taskEntity)
    }

    @Transactional(readOnly = true)
    override fun getTasks(criteriaTO: CriteriaTO<TaskFilterTO>): Set<TaskTO> {
        val criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO)
        return taskRepository.findTaskDTOSet(criteriaQuery)
            .stream()
            .map(taskMapper::taskDTOToTaskTO)
            .collect(Collectors.toCollection{
                LinkedHashSet()
            })
    }

    @Transactional(readOnly = true)
    override fun getTasksCount(criteriaTO: CriteriaTO<TaskFilterTO>): Long {
        val criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO)
        return taskRepository.countTaskDTOSet(criteriaQuery)
    }

    private fun fillTaskEntity(entity: TaskEntity, createUpdateTaskTO: CreateUpdateTaskTO) {
        entity.title = createUpdateTaskTO.title
        entity.text = createUpdateTaskTO.text
    }
}