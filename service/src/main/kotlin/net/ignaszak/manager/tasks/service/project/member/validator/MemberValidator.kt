package net.ignaszak.manager.tasks.service.project.member.validator

interface MemberValidator {
    fun valid(criteria: MemberCriteria)
}