package net.ignaszak.manager.tasks.service.project

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import net.ignaszak.manager.tasks.repository.project.ProjectRepository
import net.ignaszak.manager.tasks.repository.task.TaskRepository
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberForbiddenException
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectNotFoundException
import net.ignaszak.manager.tasks.service.project.mapper.ProjectMapper
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import net.ignaszak.manager.tasks.service.project.validator.ProjectValidator
import net.ignaszak.manager.tasks.service.task.error.TaskError.TSK_NOT_ATTACHED_TO_PROJECT
import net.ignaszak.manager.tasks.service.task.error.exception.TaskNotFoundException
import net.ignaszak.manager.tasks.servicecontract.ProjectService
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import net.ignaszak.manager.tasks.to.project.ProjectTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.ADMINISTRATOR
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
open class ProjectServiceImpl(
    private val projectRepository: ProjectRepository,
    private val projectValidator: ProjectValidator,
    private val projectMapper: ProjectMapper,
    private val memberValidator: MemberValidator,
    private val taskRepository: TaskRepository,
    private val criteriaService: CriteriaService,
    private val dateProvider: IDateTimeProvider
) : ProjectService {

    @Transactional(readOnly = true)
    override fun getById(publicId: String): ProjectTO {
        projectValidator.validPublicId(publicId)

        val projectDTO = projectRepository.findProjectDTOByPublicId(publicId)
            .orElseThrow { ProjectNotFoundException() }

        return projectMapper.projectDTOToProjectTO(projectDTO)
    }

    @Transactional
    override fun create(createUpdateProjectTO: CreateUpdateProjectTO, userPublicId: String): ProjectTO {
        projectValidator.validMemberPublicId(userPublicId)
        projectValidator.valid(createUpdateProjectTO)

        val member = ProjectMemberEntity.asCreator(userPublicId)
        val projectEntity = ProjectEntity(createUpdateProjectTO.title, member, dateProvider.getNow())

        projectRepository.save(projectEntity)

        return projectMapper.projectEntityToProjectTO(projectEntity)
    }

    @Transactional
    override fun update(createUpdateProjectTO: CreateUpdateProjectTO, projectPublicId: String): ProjectTO {
        projectValidator.validPublicId(projectPublicId)
        projectValidator.valid(createUpdateProjectTO)

        val projectEntity = getProjectEntity(projectPublicId)

        projectEntity.title = createUpdateProjectTO.title
        projectEntity.modificationDate = LocalDateTime.now()

        return projectMapper.projectEntityToProjectTO(projectEntity)
    }

    @Transactional
    override fun deleteById(publicId: String) {
        projectValidator.validPublicId(publicId)

        val projectEntity = getProjectEntity(publicId)

        projectRepository.delete(projectEntity)
    }

    @Transactional(readOnly = true)
    override fun getProjects(criteriaTO: CriteriaTO<ProjectFilterTO>): Set<ProjectTO> {
        val criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO)
        val projectDTOSet = projectRepository.findProjectDTOSet(criteriaQuery)
        return projectMapper.projectDTOSetTOProjectTOSet(projectDTOSet)
    }

    @Transactional(readOnly = true)
    override fun getProjectsCount(criteriaTO: CriteriaTO<ProjectFilterTO>): Long {
        val criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO)
        return projectRepository.countProjectDTOSet(criteriaQuery)
    }

    @Transactional
    override fun attachTask(projectPublicId: String, taskPublicId: String, userPublicId: String) {
        projectValidator.validPublicId(projectPublicId)
        projectValidator.validMemberPublicId(userPublicId)

        val projectEntity = getProjectEntity(projectPublicId)

        memberValidator.valid(
            MemberCriteria
                .user(userPublicId)
                .joinedProject(projectPublicId)
                .withAnyRole()
        )

        val taskEntity = getTaskEntity(taskPublicId)

        if (taskEntity.authorPublicId != userPublicId) {
            throw ProjectMemberForbiddenException()
        }

        taskEntity.addProject(projectEntity)
    }

    @Transactional
    override fun detachTask(projectPublicId: String, taskPublicId: String, userPublicId: String) {
        projectValidator.validPublicId(projectPublicId)
        projectValidator.validMemberPublicId(userPublicId)

        val projectEntity = getProjectEntity(projectPublicId)

        val taskEntity = getTaskEntity(taskPublicId)

        if (projectEntity.publicId != taskEntity.project?.publicId) {
            throw ValidationException(TSK_NOT_ATTACHED_TO_PROJECT)
        }

        val criteria = MemberCriteria.user(userPublicId).joinedProject(projectPublicId)
        memberValidator.valid(
            if (taskEntity.authorPublicId == userPublicId)
                criteria.withAnyRole()
            else
                criteria.withRoleEq(ADMINISTRATOR)
        )

        taskEntity.removeProject()
    }

    private fun getProjectEntity(projectPublicId: String) =
        projectRepository.findProjectEntityByPublicId(projectPublicId)
            .orElseThrow { ProjectNotFoundException() }

    private fun getTaskEntity(taskPublicId: String) = taskRepository.findTaskEntityByPublicId(taskPublicId)
        .orElseThrow { TaskNotFoundException() }
}