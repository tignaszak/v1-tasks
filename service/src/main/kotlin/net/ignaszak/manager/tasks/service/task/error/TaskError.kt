package net.ignaszak.manager.tasks.service.task.error

import net.ignaszak.manager.commons.error.Error

enum class TaskError(override val code: String, override val message: String) : Error {
    TSK_NOT_FOUND("TSK-1", "Task not found!"),
    TSK_INVALID_TITLE("TSK-2", "Invalid task title!"),
    TSK_ACCESS_FORBIDDEN("TSK-3", "Access forbidden!"),
    TSK_NOT_ATTACHED_TO_PROJECT("TSK-4", "Task is not attached to project!")
}