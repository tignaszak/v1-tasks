package net.ignaszak.manager.tasks.service.project.mapper

import net.ignaszak.manager.tasks.dto.project.ProjectDTO
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.to.project.ProjectTO
import org.mapstruct.Mapper
import org.springframework.stereotype.Component
import java.util.Collections.unmodifiableSet
import java.util.stream.Collectors

@Component
@Mapper(componentModel = "spring")
interface ProjectMapper {
    fun projectEntityToProjectTO(projectEntity: ProjectEntity): ProjectTO
    fun projectDTOToProjectTO(projectDTO: ProjectDTO): ProjectTO

    fun projectDTOSetTOProjectTOSet(projectDTOSet: Set<ProjectDTO>): Set<ProjectTO> {
        val projectTOSet = projectDTOSet.stream()
            .map(this::projectDTOToProjectTO)
            .collect(Collectors.toCollection {
                LinkedHashSet()
            })

        return unmodifiableSet(projectTOSet)
    }
}