package net.ignaszak.manager.tasks.service.project.member.validator

import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberForbiddenException
import net.ignaszak.manager.tasks.service.project.member.validator.RolePredicate.any
import net.ignaszak.manager.tasks.service.project.member.validator.RolePredicate.eq
import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role

class MemberCriteria private constructor(
    private val memberPublicId: String,
    private var rolePredicate: (Role) -> Boolean,
    private var projectPublicId: String? = null,
) {
    companion object {
        @JvmStatic
        fun user(publicId: String): MemberCriteria {
            return MemberCriteria(publicId, any())
        }
    }

    fun joinedProject(publicId: String): MemberCriteria {
        projectPublicId = publicId
        return this
    }

    fun withAnyRole(): MemberCriteria {
        rolePredicate = any()
        return this
    }

    fun withRoleEq(role: Role): MemberCriteria {
        rolePredicate = eq(role)
        return this
    }

    fun getMemberPublicId() = memberPublicId
    fun getProjectPublicId() = projectPublicId ?: throw ProjectMemberForbiddenException()
    fun getRolePredicate() = rolePredicate
}