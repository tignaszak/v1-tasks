package net.ignaszak.manager.tasks.service.project.member.validator

import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role

object RolePredicate {
    @JvmStatic
    fun any() : (Role) -> Boolean {
        return { true }
    }

    @JvmStatic
    fun eq(role: Role) : (Role) -> Boolean {
        return { it == role }
    }
}