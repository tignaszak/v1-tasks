package net.ignaszak.manager.tasks.service.project.member.validator.impl

import net.ignaszak.manager.tasks.repository.project.member.MemberRepository
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberForbiddenException
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
open class MemberValidatorImpl(private val memberRepository: MemberRepository) : MemberValidator {

    @Transactional(readOnly = true)
    override fun valid(criteria: MemberCriteria) {
        memberRepository
            .findMemberRoleByMemberPublicIdAndProjectPublicId(criteria.getMemberPublicId(), criteria.getProjectPublicId())
            .map(Role::valueOf)
            .filter(criteria.getRolePredicate())
            .orElseThrow {
                ProjectMemberForbiddenException()
            }
    }
}