package net.ignaszak.manager.tasks.service.project.member

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.tasks.dto.project.member.MemberDTO
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import net.ignaszak.manager.tasks.repository.project.ProjectRepository
import net.ignaszak.manager.tasks.repository.project.member.MemberRepository
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberForbiddenException
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberNotFound
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectNotFoundException
import net.ignaszak.manager.tasks.service.project.member.mapper.MemberMapper
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import net.ignaszak.manager.tasks.service.project.validator.ProjectValidator
import net.ignaszak.manager.tasks.to.project.member.MemberTO
import spock.lang.Specification

import static net.ignaszak.manager.tasks.service.TestHelper.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_USER_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectEntity
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectMemberCriteriaQuery
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectMemberCriteriaTO
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_INVALID_MEMBER_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_INVALID_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_EXISTS
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_FORBIDDEN
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_NOT_FOUND
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_NOT_FOUND
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.ADMINISTRATOR
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.MEMBER
import static org.springframework.util.CollectionUtils.isEmpty

class MemberServiceImplSpec extends Specification {

    private static final ProjectEntity ANY_PROJECT_ENTITY = anyProjectEntity()
    private static final String ANY_MEMBER_PUBLIC_ID = "any user public id"
    private static final ProjectMemberEntity ANY_PROJECT_MEMBER_ENTITY = anyProjectMemberEntity()

    private MemberRepository memberRepositoryMock = Mock()
    private ProjectRepository projectRepositoryMock = Mock()
    private ProjectValidator projectValidatorMock = Mock()
    private MemberMapper memberMapperMock = Mock()
    private MemberValidator memberValidatorMock = Mock()
    private CriteriaService criteriaServiceMock = Mock()

    private subject = new MemberServiceImpl(
            memberRepositoryMock,
            projectRepositoryMock,
            projectValidatorMock,
            memberMapperMock,
            memberValidatorMock,
            criteriaServiceMock
    )

    def "get project members"() {
        given:
        1 * criteriaServiceMock.buildCriteriaQuery(anyProjectMemberCriteriaTO()) >> anyProjectMemberCriteriaQuery()
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.existsProjectMemberEntityByUserPublicIdAndProject(ANY_USER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> true
        1 * memberRepositoryMock.findMemberDTOSetByProjectPublicId(ANY_PROJECT_PUBLIC_ID, anyProjectMemberCriteriaQuery()) >> anyMemberDTOSet()

        when:
        def result = subject.getMembers(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID, anyProjectMemberCriteriaTO())

        then:
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        !isEmpty(result)
    }

    def "return invalid project public id exception while getting project members"() {
        given:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID) >> {
            throw new ValidationException(PRO_INVALID_PUBLIC_ID)
        }

        when:
        subject.getMembers(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID, anyProjectMemberCriteriaTO())

        then:
        def e = thrown ValidationException
        e.getMessage() == PRO_INVALID_PUBLIC_ID.toString()

        and:
        0 * criteriaServiceMock.buildCriteriaQuery(_)
        0 * projectValidatorMock.validMemberPublicId(_)
        0 * projectRepositoryMock.findProjectEntityByPublicId(_)
        0 * memberRepositoryMock.existsProjectMemberEntityByUserPublicIdAndProject(_, _)
        0 * memberRepositoryMock.findMemberDTOSetByProjectPublicId(_)
    }

    def "return invalid user public id exception while getting project members"() {
        given:
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID) >> {
            throw new ValidationException(PRO_INVALID_MEMBER_PUBLIC_ID)
        }

        when:
        subject.getMembers(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID, anyProjectMemberCriteriaTO())

        then:
        def e = thrown ValidationException
        e.getMessage() == PRO_INVALID_MEMBER_PUBLIC_ID.toString()

        and:
        0 * criteriaServiceMock.buildCriteriaQuery(_)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * projectRepositoryMock.findProjectEntityByPublicId(_)
        0 * memberRepositoryMock.existsProjectMemberEntityByUserPublicIdAndProject(_, _)
        0 * memberRepositoryMock.findMemberDTOSetByProjectPublicId(_)
    }

    def "return project not found exception while getting project members"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.getMembers(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID, anyProjectMemberCriteriaTO())

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == PRO_NOT_FOUND.toString()

        and:
        0 * criteriaServiceMock.buildCriteriaQuery(_)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberRepositoryMock.existsProjectMemberEntityByUserPublicIdAndProject(_, _)
        0 * memberRepositoryMock.findMemberDTOSetByProjectPublicId(_)
    }

    def "return member no access exception while getting project members"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.existsProjectMemberEntityByUserPublicIdAndProject(ANY_USER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> false

        when:
        subject.getMembers(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID, anyProjectMemberCriteriaTO())

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        0 * criteriaServiceMock.buildCriteriaQuery(_)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberRepositoryMock.findMemberDTOSetByProjectPublicId(_)
    }

    def "add member to project by its admin"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.addMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * memberRepositoryMock.save({ProjectMemberEntity entity ->
            entity.userRole == ProjectMemberEntity.UserRole.MEMBER
            entity.userPublicId == ANY_MEMBER_PUBLIC_ID
            entity.project == ANY_PROJECT_ENTITY
        })

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_ENTITY.publicId
            criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "thrown an exception when project not exists while adding member"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.addMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == PRO_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(_, _)
        0 * memberRepositoryMock.save(_)
    }

    def "thrown an exception when user is not a member while adding member to project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberValidatorMock.valid(_) >> {
            throw new ProjectMemberForbiddenException()
        }
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.addMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.save(_)
    }

    def "thrown an exception when logged user is not a project admin while adding a new member"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberValidatorMock.valid(_) >> {
            throw new ProjectMemberForbiddenException()
        }
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.addMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.save(_)
    }

    def "update project member by administrator" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        def memberEntity = new ProjectMemberEntity(1, ANY_MEMBER_PUBLIC_ID, ProjectMemberEntity.UserRole.ADMINISTRATOR, false, ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.of(memberEntity)
        1 * memberMapperMock.memberTORoleToMemberEntityRole(MEMBER) >> ProjectMemberEntity.UserRole.MEMBER
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.updateMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        memberEntity.userRole == ProjectMemberEntity.UserRole.MEMBER
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_ENTITY.publicId
            criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "throw an exception while trying to update project member by other member" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberValidatorMock.valid(_) >> {
            throw new ProjectMemberForbiddenException()
        }
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.updateMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberMapperMock.memberTORoleToMemberEntityRole(_)
    }

    def "throw an exception while trying to update project member by non existing user" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberValidatorMock.valid(_) >> {
            throw new ProjectMemberForbiddenException()
        }
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.updateMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberMapperMock.memberTORoleToMemberEntityRole(_)
    }

    def "throw an exception while trying to update member in non existing project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.updateMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == PRO_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberMapperMock.memberTORoleToMemberEntityRole(_)
        0 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(_, _)
    }

    def "throw an exception while trying to update non existing project member" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_ENTITY) >>Optional.empty()
        def memberTO = new MemberTO(ANY_MEMBER_PUBLIC_ID, MEMBER)

        when:
        subject.updateMember(memberTO, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberNotFound
        e.getMessage() == PRO_MEMBER_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberMapperMock.memberTORoleToMemberEntityRole(_)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_ENTITY.publicId
            criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "delete project member by administrator" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        def memberEntity = new ProjectMemberEntity(1, ANY_MEMBER_PUBLIC_ID, ProjectMemberEntity.UserRole.ADMINISTRATOR, false, ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.of(memberEntity)

        when:
        subject.deleteMember(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * memberRepositoryMock.delete(memberEntity)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_ENTITY.publicId
            criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "throw an exception while trying to delete project member by other member" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberValidatorMock.valid(_) >> {
            throw new ProjectMemberForbiddenException()
        }

        when:
        subject.deleteMember(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.delete(_)
    }

    def "throw an exception while trying to delete project creator by administrator" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        def memberEntity = new ProjectMemberEntity(1, ANY_MEMBER_PUBLIC_ID, ProjectMemberEntity.UserRole.ADMINISTRATOR, true, ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.of(memberEntity)

        when:
        subject.deleteMember(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.delete(memberEntity)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_ENTITY.publicId
            criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "throw an exception while trying to delete project member by non existing user" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberValidatorMock.valid(_) >> {
            throw new ProjectMemberForbiddenException()
        }

        when:
        subject.deleteMember(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.delete(_)
    }

    def "throw an exception while trying to delete member in non existing project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.deleteMember(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == PRO_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.delete(_)
        0 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(_, _)
    }

    def "throw an exception while trying to delete non existing project member" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.empty()

        when:
        subject.deleteMember(ANY_MEMBER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberNotFound
        e.getMessage() == PRO_MEMBER_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validMemberPublicId(ANY_MEMBER_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * memberRepositoryMock.delete(_)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_ENTITY.publicId
            criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "join user to project as member" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_USER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.empty()

        when:
        subject.joinProject(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * memberRepositoryMock.save({ ProjectMemberEntity entity ->
            entity.userPublicId == ANY_USER_PUBLIC_ID
            entity.userRole == ProjectMemberEntity.UserRole.MEMBER
            entity.project == ANY_PROJECT_ENTITY
            !entity.isProjectCreator()
        })
    }

    def "throw an exception while trying to join project by already added user" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_USER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.of(ANY_PROJECT_MEMBER_ENTITY)

        when:
        subject.joinProject(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ValidationException
        e.getMessage() == PRO_MEMBER_EXISTS.toString()

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberRepositoryMock.save(_)
    }

    def "throw an exception while trying to join no existing project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.joinProject(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == PRO_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberRepositoryMock.save(_)
        0 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(_, _)
    }

    def "leave user from project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_USER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.of(ANY_PROJECT_MEMBER_ENTITY)

        when:
        subject.leaveProject(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * memberRepositoryMock.delete(ANY_PROJECT_MEMBER_ENTITY)
    }

    def "throw an exception while trying to leave project by its creator" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)
        1 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(ANY_USER_PUBLIC_ID, ANY_PROJECT_ENTITY) >> Optional.of(anyProjectMemberEntity(ProjectMemberEntity.UserRole.ADMINISTRATOR, true))

        when:
        subject.leaveProject(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == PRO_MEMBER_FORBIDDEN.toString()

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberRepositoryMock.delete(_)
    }

    def "throw an exception while trying to leave no existing project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.leaveProject(ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == PRO_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberRepositoryMock.delete(_)
        0 * memberRepositoryMock.findProjectMemberEntityByUserPublicIdAndProject(_, _)
    }

    private static Set<MemberDTO> anyMemberDTOSet() {
        Set.of(anyMemberDTO())
    }

    private static MemberDTO anyMemberDTO() {
        new MemberDTO(ANY_USER_PUBLIC_ID, ADMINISTRATOR.name())
    }

    private static ProjectMemberEntity anyProjectMemberEntity(
            ProjectMemberEntity.UserRole role = ProjectMemberEntity.UserRole.ADMINISTRATOR,
            Boolean isProjectCreator = false
    ) {
        new ProjectMemberEntity(1, ANY_MEMBER_PUBLIC_ID, role, isProjectCreator, ANY_PROJECT_ENTITY)
    }
}
