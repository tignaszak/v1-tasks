package net.ignaszak.manager.tasks.service.project

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import net.ignaszak.manager.tasks.repository.project.ProjectRepository
import net.ignaszak.manager.tasks.repository.task.TaskRepository
import net.ignaszak.manager.tasks.service.project.error.ProjectError
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberForbiddenException
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectNotFoundException
import net.ignaszak.manager.tasks.service.project.mapper.ProjectMapper
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import net.ignaszak.manager.tasks.service.project.validator.ProjectValidator
import net.ignaszak.manager.tasks.service.task.error.TaskError
import net.ignaszak.manager.tasks.service.task.error.exception.TaskNotFoundException
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import spock.lang.Specification

import java.time.LocalDateTime

import static net.ignaszak.manager.tasks.service.TestHelper.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_PROJECT_TITLE
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_USER_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.anyCreateUpdateProjectTO
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectDTO
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectDTOSet
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectEntity
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectFilterCriteriaQuery
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectFilterCriteriaTO
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectTO
import static net.ignaszak.manager.tasks.service.TestHelper.anyTaskEntity
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.ADMINISTRATOR
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.MEMBER
import static org.springframework.util.CollectionUtils.isEmpty

class ProjectServiceImplSpec extends Specification {

    public static final String ANY_OTHER_USER_PUBLIC_Id = "other user public id"

    private ProjectRepository projectRepositoryMock = Mock()
    private ProjectValidator projectValidatorMock = Mock()
    private ProjectMapper projectMapperMock = Mock()
    private MemberValidator memberValidatorMock = Mock()
    private TaskRepository taskRepositoryMock = Mock()
    private CriteriaService criteriaServiceMock = Mock()
    private IDateTimeProvider dateProvider = LocalDateTime::now

    private subject = new ProjectServiceImpl(
            projectRepositoryMock,
            projectValidatorMock,
            projectMapperMock,
            memberValidatorMock,
            taskRepositoryMock,
            criteriaServiceMock,
            dateProvider
    )

    def cleanup() {
        anyTaskEntity().project = null
    }

    def "return 'ProjectTO' on successful project creation"() {
        given:
        1 * projectMapperMock.projectEntityToProjectTO(_ as ProjectEntity) >> anyProjectTO()

        when:
        def result = subject.create(anyCreateUpdateProjectTO(), ANY_USER_PUBLIC_ID)

        then:
        result == anyProjectTO()
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * projectValidatorMock.valid(anyCreateUpdateProjectTO())
        1 * projectRepositoryMock.save({ ProjectEntity entity ->
            !entity.publicId.isBlank()
            entity.title == ANY_PROJECT_TITLE
            entity.creationDate != null
            entity.modificationDate == null
            !isEmpty(entity.members)
            entity.members.stream()
                .filter {
                    ANY_USER_PUBLIC_ID == it.userPublicId
                    && ProjectMemberEntity.UserRole.ADMINISTRATOR == it.userRole
                    && it.isProjectCreator()
                }
                .findAny()
                .isPresent()
        })
    }

    def "throw an exception on invalid model while task creation"() {
        given:
        1 * projectValidatorMock.valid(_ as CreateUpdateProjectTO) >> {
            throw new ValidationException(ProjectError.PRO_INVALID_TITLE)
        }

        when:
        subject.create(anyCreateUpdateProjectTO(), ANY_USER_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * projectRepositoryMock.save(_)
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_TITLE.toString()
    }

    def "throw an exception on invalid member public id while project creation"() {
        given:
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID) >> {
            throw new ValidationException(ProjectError.PRO_INVALID_MEMBER_PUBLIC_ID)
        }

        when:
        subject.create(anyCreateUpdateProjectTO(), ANY_USER_PUBLIC_ID)

        then:
        0 * projectValidatorMock.valid(_)
        0 * projectRepositoryMock.save(_)
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_MEMBER_PUBLIC_ID.toString()
    }

    def "return 'ProjectTO' by project public id"() {
        given:
        1 * projectRepositoryMock.findProjectDTOByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(anyProjectDTO())
        1 * projectMapperMock.projectDTOToProjectTO(anyProjectDTO()) >> anyProjectTO()

        when:
        def result = subject.getById(ANY_PROJECT_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validPublicId(_ as String)
        !result.publicId.isBlank()
        result.publicId == ANY_PROJECT_PUBLIC_ID
    }

    def "throw an exception when project does not exists while getting project by public id"() {
        given:
        1 * projectRepositoryMock.findProjectDTOByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.getById(ANY_PROJECT_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validPublicId(_ as String)
        0 * projectMapperMock.projectDTOToProjectTO(_)
        def e = thrown ProjectNotFoundException
        e.getMessage() == ProjectError.PRO_NOT_FOUND.toString()
    }

    def "throw an exception while getting project by invalid public id"() {
        given:
        def invalidPublicId = "invalidPublicId"
        1 * projectValidatorMock.validPublicId(invalidPublicId) >> {
            throw new ValidationException(ProjectError.PRO_INVALID_PUBLIC_ID)
        }

        when:
        subject.getById(invalidPublicId)

        then:
        0 * projectRepositoryMock.findProjectDTOByPublicId(_)
        0 * projectMapperMock.projectDTOToProjectTO(_)
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_PUBLIC_ID.toString()
    }

    def "get all projects"() {
        given:
        1 * criteriaServiceMock.buildCriteriaQuery(anyProjectFilterCriteriaTO()) >> anyProjectFilterCriteriaQuery()
        1 * projectRepositoryMock.findProjectDTOSet(anyProjectFilterCriteriaQuery()) >> anyProjectDTOSet()
        1 * projectMapperMock.projectDTOSetTOProjectTOSet(anyProjectDTOSet()) >> Set.of(anyProjectTO())

        when:
        def result = subject.getProjects(anyProjectFilterCriteriaTO())

        then:
        result != null
        result.size() == 1
        result[0] == anyProjectTO()
    }

    def "get empty set of projects"() {
        given:
        def emptySet = Set.of()
        1 * criteriaServiceMock.buildCriteriaQuery(anyProjectFilterCriteriaTO()) >> anyProjectFilterCriteriaQuery()
        1 * projectRepositoryMock.findProjectDTOSet(anyProjectFilterCriteriaQuery()) >> emptySet
        1 * projectMapperMock.projectDTOSetTOProjectTOSet(emptySet) >> Set.of()

        when:
        def result = subject.getProjects(anyProjectFilterCriteriaTO())

        then:
        result != null
        result.size() == 0
    }

    def "return 'ProjectTO' while successful project update"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(anyProjectEntity())

        when:
        subject.update(anyCreateUpdateProjectTO(), ANY_PROJECT_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.valid(anyCreateUpdateProjectTO())
        1 * projectMapperMock.projectEntityToProjectTO({ProjectEntity entity ->
            entity.modificationDate != null
        })
    }

    def "throw an exception when public id is invalid while updating project"() {
        given:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID) >> {
            throw new ValidationException(ProjectError.PRO_INVALID_PUBLIC_ID)
        }

        when:
        subject.update(anyCreateUpdateProjectTO(), ANY_PROJECT_PUBLIC_ID)

        then:
        0 * projectValidatorMock.valid(_)
        0 * projectRepositoryMock.findProjectEntityByPublicId(_)
        0 * projectMapperMock.projectEntityToProjectTO(_)
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_PUBLIC_ID.toString()
    }

    def "throw an exception while updating project with invalid data"() {
        given:
        1 * projectValidatorMock.valid(anyCreateUpdateProjectTO()) >> {
            throw new ValidationException(ProjectError.PRO_INVALID_TITLE)
        }

        when:
        subject.update(anyCreateUpdateProjectTO(), ANY_PROJECT_PUBLIC_ID)

        then:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        0 * projectRepositoryMock.findProjectEntityByPublicId(_)
        0 * projectMapperMock.projectEntityToProjectTO(_)
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_TITLE.toString()
    }

    def "throw an exception while updating no existing project"() {
        given:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.valid(anyCreateUpdateProjectTO())
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.update(anyCreateUpdateProjectTO(), ANY_PROJECT_PUBLIC_ID)

        then:
        0 * projectMapperMock.projectEntityToProjectTO(_)
        def e = thrown ProjectNotFoundException
        e.getMessage() == ProjectError.PRO_NOT_FOUND.toString()
    }

    def "successful delete project by public id"() {
        given:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(anyProjectEntity())

        when:
        subject.deleteById(ANY_PROJECT_PUBLIC_ID)

        then:
        noExceptionThrown()
    }

    def "throw an exception while deleting project with invalid public id"() {
        given:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID) >> {
            throw new ValidationException(ProjectError.PRO_INVALID_PUBLIC_ID)
        }

        when:
        subject.deleteById(ANY_PROJECT_PUBLIC_ID)

        then:
        0 * projectRepositoryMock.findProjectEntityByPublicId(_)
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_PUBLIC_ID.toString()
    }

    def "throw an exception while deleting no existing project"() {
        given:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.deleteById(ANY_PROJECT_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == ProjectError.PRO_NOT_FOUND.toString()
    }

    def "attach task to project"() {
        given:
        def taskEntity = anyTaskEntity()
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(taskEntity)
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(anyProjectEntity())

        when:
        subject.attachTask(ANY_PROJECT_PUBLIC_ID, ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        noExceptionThrown()
        taskEntity.project instanceof ProjectEntity

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_PUBLIC_ID
            criteria.rolePredicate.invoke(MEMBER) && criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "throw an exception while attaching task to no existing project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.attachTask(ANY_PROJECT_PUBLIC_ID, ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == ProjectError.PRO_NOT_FOUND.toString()

        and:
        0 * taskRepositoryMock.findTaskEntityByPublicId(_)
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberValidatorMock.valid(_)
    }

    def "throw an exception while attaching task to project when task not exists" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(anyProjectEntity())
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.empty()

        when:
        subject.attachTask(ANY_PROJECT_PUBLIC_ID, ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown TaskNotFoundException
        e.getMessage() == TaskError.TSK_NOT_FOUND.toString()

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_PUBLIC_ID
            criteria.rolePredicate.invoke(MEMBER) && criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "throw an exception when user is not task author while attaching task to project" () {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(anyProjectEntity())
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(anyTaskEntity())

        when:
        subject.attachTask(ANY_PROJECT_PUBLIC_ID, ANY_TASK_PUBLIC_ID, ANY_OTHER_USER_PUBLIC_Id)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == ProjectError.PRO_MEMBER_FORBIDDEN.toString()
        anyTaskEntity().project == null

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_OTHER_USER_PUBLIC_Id)
        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_OTHER_USER_PUBLIC_Id
            criteria.projectPublicId == ANY_PROJECT_PUBLIC_ID
            criteria.rolePredicate.invoke(MEMBER) && criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "detach tasks" () {
        given:
        def projectEntity = anyProjectEntity()
        def taskEntity = anyTaskEntity()
        taskEntity.project = projectEntity
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(projectEntity)
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(taskEntity)

        when:
        subject.detachTask(ANY_PROJECT_PUBLIC_ID, ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        noExceptionThrown()
        taskEntity.project == null

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        1 * memberValidatorMock.valid({MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_PUBLIC_ID
            criteria.rolePredicate.invoke(MEMBER) && criteria.rolePredicate.invoke(ADMINISTRATOR)
        })
    }

    def "throw an exception while detaching task from project by other member" () {
        given:
        def projectEntity = anyProjectEntity()
        def taskEntity = anyTaskEntity()
        taskEntity.project = projectEntity
        1 * memberValidatorMock.valid({MemberCriteria criteria ->
            criteria.memberPublicId == ANY_OTHER_USER_PUBLIC_Id
            criteria.projectPublicId == ANY_PROJECT_PUBLIC_ID
            criteria.rolePredicate.invoke(ADMINISTRATOR)
        }) >> {
            throw new ProjectMemberForbiddenException()
        }
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(projectEntity)
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(taskEntity)

        when:
        subject.detachTask(ANY_PROJECT_PUBLIC_ID, ANY_TASK_PUBLIC_ID, ANY_OTHER_USER_PUBLIC_Id)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == ProjectError.PRO_MEMBER_FORBIDDEN.toString()
        taskEntity.project == projectEntity

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_OTHER_USER_PUBLIC_Id)
    }

    def "throw an exception while detaching task when no project is attached"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(anyProjectEntity())
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(anyTaskEntity())

        when:
        subject.detachTask(ANY_PROJECT_PUBLIC_ID, ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_NOT_ATTACHED_TO_PROJECT.toString()

        and:
        1 * projectValidatorMock.validPublicId(ANY_PROJECT_PUBLIC_ID)
        1 * projectValidatorMock.validMemberPublicId(ANY_USER_PUBLIC_ID)
        0 * memberValidatorMock.valid(_)
    }
}
