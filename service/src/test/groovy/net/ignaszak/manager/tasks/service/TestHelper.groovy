package net.ignaszak.manager.tasks.service

import net.ignaszak.manager.commons.restcriteria.paging.query.PageQuery
import net.ignaszak.manager.commons.restcriteria.paging.to.PageCriteriaTO
import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.dto.project.ProjectDTO
import net.ignaszak.manager.tasks.dto.task.TaskDTO
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import net.ignaszak.manager.tasks.entity.TaskEntity
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import net.ignaszak.manager.tasks.to.project.ProjectTO
import net.ignaszak.manager.tasks.to.project.member.MemberFilterTO
import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO
import net.ignaszak.manager.tasks.to.task.TaskFilterTO
import net.ignaszak.manager.tasks.to.task.TaskTO

import java.time.LocalDateTime

import static net.ignaszak.manager.tasks.entity.ProjectMemberEntity.UserRole.ADMINISTRATOR

class TestHelper {

    static final LocalDateTime ANY_DATE = LocalDateTime.now()

    static final String ANY_TASK_PUBLIC_ID = "997037ec-9733-407c-ad1d-589b6b88486c"
    static final String ANY_TASK_TEXT = "task text"
    static final String ANY_TASK_TITLE = "task title"
    static final String ANY_USER_PUBLIC_ID = "4b4a0d81-891b-403f-a9cf-5a737cbe15af"

    static final String ANY_PROJECT_PUBLIC_ID = "2fe38d9b-03e3-4692-9f92-7955fd596163"
    static final String ANY_PROJECT_TITLE = "project title"

    static TaskEntity anyTaskEntity() {
        new TaskEntity(ANY_USER_PUBLIC_ID, ANY_TASK_TITLE, ANY_TASK_TEXT, ANY_DATE, ANY_DATE, null)
    }

    static TaskDTO anyTaskDTO() {
        new TaskDTO(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID, ANY_TASK_TITLE, ANY_TASK_TEXT, ANY_DATE, ANY_DATE)
    }

    static CreateUpdateTaskTO anyCreateUpdateTaskTO() {
        new CreateUpdateTaskTO(ANY_TASK_TITLE, ANY_TASK_TEXT)
    }

    static TaskTO anyTaskTO() {
        new TaskTO(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID, ANY_TASK_TITLE, ANY_TASK_TEXT, ANY_DATE, ANY_DATE)
    }

    static ProjectEntity anyProjectEntity() {
        def set = new HashSet()
        set.add(anyProjectMemberEntity())
        new ProjectEntity(ANY_PROJECT_TITLE, set, ANY_DATE, ANY_DATE, Set.of())
    }

    static CreateUpdateProjectTO anyCreateUpdateProjectTO() {
        new CreateUpdateProjectTO(ANY_PROJECT_TITLE)
    }

    static ProjectTO anyProjectTO() {
        new ProjectTO(ANY_PROJECT_PUBLIC_ID, ANY_PROJECT_TITLE, ANY_DATE, ANY_DATE)
    }

    static ProjectDTO anyProjectDTO() {
        new ProjectDTO(ANY_PROJECT_PUBLIC_ID, ANY_PROJECT_TITLE, ANY_DATE, ANY_DATE)
    }

    static Set<ProjectDTO> anyProjectDTOSet() {
        Set.of(anyProjectDTO())
    }

    static ProjectMemberEntity anyProjectMemberEntity() {
        ADMINISTRATOR.of(ANY_USER_PUBLIC_ID)
    }

    static PageCriteriaTO anyPageCriteriaTO() {
        new PageCriteriaTO(1)
    }

    static CriteriaTO<MemberFilterTO> anyProjectMemberCriteriaTO() {
        new CriteriaTO(anyPageCriteriaTO(), anyMemberFilterTO())
    }

    static MemberFilterTO anyMemberFilterTO() {
        new MemberFilterTO(null, null)
    }

    static CriteriaTO<ProjectFilterTO> anyProjectFilterCriteriaTO() {
        new CriteriaTO(anyPageCriteriaTO(), anyProjectFilterTO())
    }

    static CriteriaQuery<ProjectFilterTO> anyProjectFilterCriteriaQuery() {
        new CriteriaQuery(anyPageQuery(), anyProjectFilterTO())
    }

    static ProjectFilterTO anyProjectFilterTO() {
        new ProjectFilterTO(null, null, null, null)
    }

    static CriteriaTO<TaskFilterTO> anyTaskFilterCriteriaTo() {
        new CriteriaTO(anyPageCriteriaTO(), anyTaskFilterTO())
    }

    static CriteriaQuery<TaskFilterTO> anyTaskFilterCriteriaQuery() {
        new CriteriaQuery(anyPageQuery(), anyTaskFilterTO())
    }

    static TaskFilterTO anyTaskFilterTO() {
        new TaskFilterTO(null, null, null, null, null, null, null)
    }

    static PageQuery anyPageQuery() {
        new PageQuery(20, 0)
    }

    static CriteriaQuery<MemberFilterTO> anyProjectMemberCriteriaQuery() {
        new CriteriaQuery(anyPageQuery(), anyMemberFilterTO())
    }

    private TestHelper() {}
}
