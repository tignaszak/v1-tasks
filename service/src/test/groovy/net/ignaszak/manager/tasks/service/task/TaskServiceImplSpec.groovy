package net.ignaszak.manager.tasks.service.task

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tasks.dto.task.TaskDTO
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.TaskEntity
import net.ignaszak.manager.tasks.repository.project.ProjectRepository
import net.ignaszak.manager.tasks.repository.task.TaskRepository
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectNotFoundException
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import net.ignaszak.manager.tasks.service.task.error.TaskError
import net.ignaszak.manager.tasks.service.task.error.exception.TaskNotFoundException
import net.ignaszak.manager.tasks.service.task.mapper.TaskMapper
import net.ignaszak.manager.tasks.service.task.validator.TaskValidator
import net.ignaszak.manager.tasks.servicecontract.TaskService
import net.ignaszak.manager.tasks.to.task.TaskTO
import spock.lang.Specification

import java.time.LocalDateTime

import static net.ignaszak.manager.tasks.service.TestHelper.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_TASK_TEXT
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_TASK_TITLE
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_USER_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.anyCreateUpdateTaskTO
import static net.ignaszak.manager.tasks.service.TestHelper.anyProjectEntity
import static net.ignaszak.manager.tasks.service.TestHelper.anyTaskDTO
import static net.ignaszak.manager.tasks.service.TestHelper.anyTaskEntity
import static net.ignaszak.manager.tasks.service.TestHelper.anyTaskFilterCriteriaQuery
import static net.ignaszak.manager.tasks.service.TestHelper.anyTaskFilterCriteriaTo
import static net.ignaszak.manager.tasks.service.TestHelper.anyTaskTO
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_NOT_FOUND
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.ADMINISTRATOR
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.MEMBER

class TaskServiceImplSpec extends Specification {

    private static final TaskEntity ANY_TASK_ENTITY = anyTaskEntity()
    private static final TaskDTO ANY_TASK_DTO = anyTaskDTO()
    private static final TaskTO ANY_TASK_TO = anyTaskTO()
    private static final ProjectEntity ANY_PROJECT_ENTITY = anyProjectEntity()

    private TaskService subject

    private MemberValidator memberValidatorMock = Mock()
    private TaskRepository taskRepositoryMock = Mock()
    private ProjectRepository projectRepositoryMock = Mock()
    private TaskValidator taskValidatorMock = Mock()
    private TaskMapper taskMapperMock = Mock()
    private CriteriaService criteriaServiceMock = Mock()

    private IDateTimeProvider dateProvider = new IDateTimeProvider() {
        @Override
        LocalDateTime getNow() {
            return LocalDateTime.now()
        }
    }

    def setup() {
        subject = new TaskServiceImpl(
                memberValidatorMock,
                taskRepositoryMock,
                projectRepositoryMock,
                taskValidatorMock,
                taskMapperMock,
                criteriaServiceMock,
                dateProvider
        )
    }

    def "return 'TaskTO' when tasks exists and user is task author"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(ANY_TASK_ENTITY)
        1 * taskMapperMock.taskEntityToTaskTO(ANY_TASK_ENTITY) >> ANY_TASK_TO

        when:
        def result = subject.getById(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * taskValidatorMock.validAuthorPublicId(_, _)
        assert !result.publicId.isBlank()
        assert result.authorPublicId == ANY_USER_PUBLIC_ID
    }

    def "throw exception if task does not exists while getting task"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.empty()

        when:
        subject.getById(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        0 * taskValidatorMock.validAuthorPublicId(_, _)
        def e = thrown TaskNotFoundException
        e.getMessage() == TaskError.TSK_NOT_FOUND.toString()
    }

    def "throw exception if user is not task author while getting task"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(ANY_TASK_ENTITY)
        1 * taskValidatorMock.validAuthorPublicId(ANY_USER_PUBLIC_ID, ANY_USER_PUBLIC_ID) >> {
            throw new ValidationException(TaskError.TSK_ACCESS_FORBIDDEN)
        }

        when:
        subject.getById(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_ACCESS_FORBIDDEN.toString()
    }

    def "return 'TaskTO' on successful task creation"() {
        given:
        1 * taskMapperMock.taskEntityToTaskTO(_ as TaskEntity) >> ANY_TASK_TO

        when:
        def result = subject.create(anyCreateUpdateTaskTO(), ANY_USER_PUBLIC_ID)

        then:
        result == ANY_TASK_TO
        1 * taskValidatorMock.valid(anyCreateUpdateTaskTO())
        1 * taskRepositoryMock.save({TaskEntity entity ->
            entity instanceof TaskEntity
            !entity.publicId.isBlank()
            entity.authorPublicId == ANY_USER_PUBLIC_ID
            entity.title == ANY_TASK_TITLE
            entity.text == ANY_TASK_TEXT
            entity.creationDate != null
            entity.modificationDate == null
            entity.project == null
        })
    }

    def "throw exception on invalid model while task creation"() {
        given:
        1 * taskValidatorMock.valid(anyCreateUpdateTaskTO()) >> {
            throw new ValidationException(TaskError.TSK_INVALID_TITLE)
        }

        when:
        subject.create(anyCreateUpdateTaskTO(), ANY_USER_PUBLIC_ID)

        then:
        0 * taskRepositoryMock.save(_)
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_INVALID_TITLE.toString()
    }

    def "return 'TaskTO' on successful task creation for given project"() {
        given:
        1 * taskMapperMock.taskEntityToTaskTO(_ as TaskEntity) >> ANY_TASK_TO
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.of(ANY_PROJECT_ENTITY)

        when:
        def result = subject.create(anyCreateUpdateTaskTO(), ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        result == ANY_TASK_TO

        1 * memberValidatorMock.valid({ MemberCriteria criteria ->
            criteria.memberPublicId == ANY_USER_PUBLIC_ID
            criteria.projectPublicId == ANY_PROJECT_PUBLIC_ID
            criteria.rolePredicate.invoke(ADMINISTRATOR) && criteria.rolePredicate.invoke(MEMBER)
        })
        1 * taskValidatorMock.valid(anyCreateUpdateTaskTO())
        1 * taskRepositoryMock.save({TaskEntity entity ->
            entity instanceof TaskEntity
            !entity.publicId.isBlank()
            entity.authorPublicId == ANY_USER_PUBLIC_ID
            entity.title == ANY_TASK_TITLE
            entity.text == ANY_TASK_TEXT
            entity.creationDate != null
            entity.modificationDate == null
            entity.project == ANY_PROJECT_ENTITY
        })
    }

    def "throw an exception while task creation for no existing project"() {
        given:
        1 * projectRepositoryMock.findProjectEntityByPublicId(ANY_PROJECT_PUBLIC_ID) >> Optional.empty()

        when:
        subject.create(anyCreateUpdateTaskTO(), ANY_PROJECT_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        def e = thrown ProjectNotFoundException
        e.getMessage() == PRO_NOT_FOUND.toString()

        and:
        0 * taskMapperMock.taskEntityToTaskTO(_)
        0 * memberValidatorMock.valid(_, _, _)
        1 * taskValidatorMock.valid(anyCreateUpdateTaskTO())
        0 * taskRepositoryMock.save(_)
    }

    def "return 'TaskTO' on successful task update"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(ANY_TASK_ENTITY)

        when:
        subject.update(anyCreateUpdateTaskTO(), ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * taskValidatorMock.valid(anyCreateUpdateTaskTO())
        1 * taskValidatorMock.validAuthorPublicId(ANY_TASK_ENTITY.authorPublicId, ANY_USER_PUBLIC_ID)
        1 * taskMapperMock.taskEntityToTaskTO({TaskEntity entity ->
            entity instanceof TaskEntity
            !entity.publicId.isBlank()
            entity.authorPublicId == ANY_USER_PUBLIC_ID
            entity.title == ANY_TASK_TITLE
            entity.text == ANY_TASK_TEXT
            entity.creationDate != null
            entity.creationDate != entity.modificationDate
        })
    }

    def "throw exception if user is not task author while task update"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(ANY_TASK_ENTITY)
        1 * taskValidatorMock.validAuthorPublicId(ANY_USER_PUBLIC_ID, ANY_USER_PUBLIC_ID) >> {
            throw new ValidationException(TaskError.TSK_ACCESS_FORBIDDEN)
        }

        when:
        subject.update(anyCreateUpdateTaskTO(), ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * taskValidatorMock.valid(anyCreateUpdateTaskTO())
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_ACCESS_FORBIDDEN.toString()
    }

    def "throw exception on invalid TO while task update"() {
        given:
        1 * taskValidatorMock.valid(anyCreateUpdateTaskTO()) >> {
            throw new ValidationException(TaskError.TSK_INVALID_TITLE)
        }

        when:
        subject.update(anyCreateUpdateTaskTO(), ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        0 * taskRepositoryMock.findTaskEntityByPublicId(_)
        0 * taskValidatorMock.validAuthorPublicId(_, _)
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_INVALID_TITLE.toString()
    }

    def "successful delete task when task exists and user is task author"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(ANY_TASK_ENTITY)

        when:
        subject.deleteById(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        1 * taskValidatorMock.validAuthorPublicId(ANY_USER_PUBLIC_ID, ANY_USER_PUBLIC_ID)
        1 * taskRepositoryMock.delete(ANY_TASK_ENTITY)
        noExceptionThrown()
    }

    def "throw exception when task does not exists while task deletion"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.empty()

        when:
        subject.deleteById(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        0 * taskValidatorMock.validAuthorPublicId(_, _)
        0 * taskRepositoryMock.delete(_)
        def e = thrown TaskNotFoundException
        e.getMessage() == TaskError.TSK_NOT_FOUND.toString()
    }

    def "throw exception when user is not task author while task deletion"() {
        given:
        1 * taskRepositoryMock.findTaskEntityByPublicId(ANY_TASK_PUBLIC_ID) >> Optional.of(ANY_TASK_ENTITY)
        1 * taskValidatorMock.validAuthorPublicId(ANY_TASK_ENTITY.authorPublicId, ANY_USER_PUBLIC_ID) >> {
            throw new ValidationException(TaskError.TSK_ACCESS_FORBIDDEN)
        }

        when:
        subject.deleteById(ANY_TASK_PUBLIC_ID, ANY_USER_PUBLIC_ID)

        then:
        0 * taskRepositoryMock.delete(_)
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_ACCESS_FORBIDDEN.toString()
    }

    def "get all tasks" () {
        given:
        1 * criteriaServiceMock.buildCriteriaQuery(anyTaskFilterCriteriaTo()) >> anyTaskFilterCriteriaQuery()
        1 * taskRepositoryMock.findTaskDTOSet(anyTaskFilterCriteriaQuery()) >> Set.of(ANY_TASK_DTO)
        1 * taskMapperMock.taskDTOToTaskTO(ANY_TASK_DTO) >> ANY_TASK_TO

        when:
        def result= subject.getTasks(anyTaskFilterCriteriaTo())

        then:
        result != null
        result.size() == 1
        result.contains(ANY_TASK_TO)
    }
}
