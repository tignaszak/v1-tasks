package net.ignaszak.manager.tasks.service.project.validator.impl

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.tasks.service.project.error.ProjectError
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import spock.lang.Specification

import static net.ignaszak.manager.tasks.service.TestHelper.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_PROJECT_TITLE

class ProjectValidatorImplSpec extends Specification {

    private subject = new ProjectValidatorImpl()

    def "test fail on create update project to validation" (String title) {
        when:
        subject.valid(new CreateUpdateProjectTO(title))

        then:
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_TITLE.toString()

        where:
        title | _
        ""    | _
        " \n" | _
    }

    def "successful validation"() {
        when:
        subject.valid(new CreateUpdateProjectTO(ANY_PROJECT_TITLE))

        then:
        noExceptionThrown()
    }

    def "throw an exception on invalid public id"(String publicId) {
        when:
        subject.validPublicId(publicId)

        then:
        def e = thrown ValidationException
        e.getMessage() == ProjectError.PRO_INVALID_PUBLIC_ID.toString()

        where:
        publicId                         | _
        ""                               | _
        "sdfdsf-gfdgfdg-dsgdsfg-dsgsdfg" | _
    }

    def "valid public id"() {
        when:
        subject.validPublicId(ANY_PROJECT_PUBLIC_ID)

        then:
        noExceptionThrown()
    }
}
