package net.ignaszak.manager.tasks.service.task.validator.impl

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.tasks.service.task.error.TaskError
import net.ignaszak.manager.tasks.service.task.validator.TaskValidator
import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO
import spock.lang.Specification

import static net.ignaszak.manager.tasks.service.TestHelper.ANY_TASK_TEXT
import static net.ignaszak.manager.tasks.service.TestHelper.anyCreateUpdateTaskTO

class TaskValidatorImplSpec extends Specification {

    private TaskValidator taskValidator

    def setup() {
        taskValidator = new TaskValidatorImpl()
    }

    def "no exception is thrown when all required fields are correct"() {
        given:
        def createUpdateTaskTO = anyCreateUpdateTaskTO()

        when:
        taskValidator.valid(createUpdateTaskTO)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when title is empty"() {
        given:
        def createUpdateTaskTO = anyCreateUpdateTaskTOWithEmptyTitle()

        when:
        taskValidator.valid(createUpdateTaskTO)

        then:
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_INVALID_TITLE.toString()
    }

    def "no exception is thrown when public ids equals"() {
        given:
        def authorPublicId = "publicId"
        def userPublicId = "publicId"

        when:
        taskValidator.validAuthorPublicId(authorPublicId, userPublicId)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when ids are not equals"() {
        given:
        def authorPublicId = "authorPublicId"
        def userPublicId = "userPublicId"

        when:
        taskValidator.validAuthorPublicId(authorPublicId, userPublicId)

        then:
        def e = thrown ValidationException
        e.getMessage() == TaskError.TSK_ACCESS_FORBIDDEN.toString()
    }

    private static CreateUpdateTaskTO anyCreateUpdateTaskTOWithEmptyTitle() {
        return new CreateUpdateTaskTO("", ANY_TASK_TEXT)
    }
}
