package net.ignaszak.manager.tasks.service.project.member.validator.impl

import net.ignaszak.manager.tasks.repository.project.member.MemberRepository
import net.ignaszak.manager.tasks.service.project.error.ProjectError
import net.ignaszak.manager.tasks.service.project.error.exception.ProjectMemberForbiddenException
import net.ignaszak.manager.tasks.service.project.member.validator.MemberCriteria
import net.ignaszak.manager.tasks.service.project.member.validator.MemberValidator
import spock.lang.Specification

import static net.ignaszak.manager.tasks.service.TestHelper.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.TestHelper.ANY_USER_PUBLIC_ID
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.ADMINISTRATOR
import static net.ignaszak.manager.tasks.to.project.member.MemberTO.Role.MEMBER

class MemberValidatorImplSpec extends Specification {

    private MemberRepository memberRepositoryMock = Mock()

    private MemberValidator subject = new MemberValidatorImpl(memberRepositoryMock)

    def "valid member"() {
        given:
        1 * memberRepositoryMock.findMemberRoleByMemberPublicIdAndProjectPublicId(ANY_USER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID) >> Optional.of(MEMBER.name())
        def criteria = MemberCriteria
                .user(ANY_USER_PUBLIC_ID)
                .joinedProject(ANY_PROJECT_PUBLIC_ID)
                .withAnyRole()

        when:
        subject.valid(criteria)

        then:
        noExceptionThrown()
    }

    def "fail member validation"() {
        given:
        1 * memberRepositoryMock.findMemberRoleByMemberPublicIdAndProjectPublicId(ANY_USER_PUBLIC_ID, ANY_PROJECT_PUBLIC_ID) >> Optional.of(MEMBER.name())
        def criteria = MemberCriteria
                .user(ANY_USER_PUBLIC_ID)
                .joinedProject(ANY_PROJECT_PUBLIC_ID)
                .withRoleEq(ADMINISTRATOR)

        when:
        subject.valid(criteria)

        then:
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == ProjectError.PRO_MEMBER_FORBIDDEN.toString()
    }

    def "fail member validation with invalid criteria"() {
        given:
        def criteria = MemberCriteria
                .user(ANY_USER_PUBLIC_ID)

        when:
        subject.valid(criteria)

        then:
        0 * memberRepositoryMock.findMemberRoleByMemberPublicIdAndProjectPublicId(_, _)
        def e = thrown ProjectMemberForbiddenException
        e.getMessage() == ProjectError.PRO_MEMBER_FORBIDDEN.toString()
    }
}
