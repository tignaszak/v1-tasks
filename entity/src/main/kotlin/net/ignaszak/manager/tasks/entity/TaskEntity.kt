package net.ignaszak.manager.tasks.entity

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "task")
class TaskEntity (
    @Column(nullable = false) val authorPublicId: String,
    var title: String? = null,
    var text: String? = null,
    @Column(nullable = false) val creationDate: LocalDateTime,
    var modificationDate: LocalDateTime? = null,
    @ManyToOne var project: ProjectEntity? = null
) : PublicBaseEntity() {

    constructor(authorPublicId: String, creationDate: LocalDateTime): this(authorPublicId, null, null, creationDate)

    fun addProject(project: ProjectEntity) {
        this.project = project
    }

    fun removeProject() {
        this.project = null
    }
}