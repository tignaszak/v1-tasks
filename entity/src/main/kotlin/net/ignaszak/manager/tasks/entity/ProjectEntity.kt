package net.ignaszak.manager.tasks.entity

import java.time.LocalDateTime
import javax.persistence.*
import javax.persistence.CascadeType.*

@Entity
@Table(name = "project")
class ProjectEntity(
    @Column(nullable = false) var title: String,
    @OneToMany(mappedBy = "project", cascade = [PERSIST, REMOVE]) var members: Set<ProjectMemberEntity>,
    @Column(nullable = false) val creationDate: LocalDateTime,
    var modificationDate: LocalDateTime? = null,
    @OneToMany(mappedBy = "project") var tasks: Set<TaskEntity> = emptySet()
) : PublicBaseEntity() {
    constructor(title: String, member: ProjectMemberEntity, creationDate: LocalDateTime): this(title, setOf(member), creationDate)

    init {
        members.forEach {
            it.project = this
        }
    }

    fun addMember(member: ProjectMemberEntity) {
        member.project = this
        members.plus(member)
    }
}
