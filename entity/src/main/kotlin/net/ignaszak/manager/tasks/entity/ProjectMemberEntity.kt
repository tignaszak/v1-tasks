package net.ignaszak.manager.tasks.entity

import javax.persistence.*

@Entity
@Table(name = "project_member")
class ProjectMemberEntity(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    @Column(nullable = false) val userPublicId: String,
    @Column(nullable = false) @Enumerated(EnumType.STRING) var userRole: UserRole,
    val isProjectCreator: Boolean = false,
    @ManyToOne(optional = false) var project: ProjectEntity? = null
) {
    enum class UserRole {
        ADMINISTRATOR, MEMBER;

        fun of(userPublicId: String) = ProjectMemberEntity(null, userPublicId, this)
    }

    companion object {
        fun asCreator(userPublicId: String) : ProjectMemberEntity {
            return ProjectMemberEntity(null, userPublicId, UserRole.ADMINISTRATOR, true)
        }
    }

    fun addProject(project: ProjectEntity) {
        project.addMember(this)
    }
}