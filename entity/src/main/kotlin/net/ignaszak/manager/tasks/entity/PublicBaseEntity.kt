package net.ignaszak.manager.tasks.entity

import java.util.*
import javax.persistence.*

@MappedSuperclass
abstract class PublicBaseEntity (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) open val id: Long? = null,
    @Column(nullable = false) open val publicId: String = UUID.randomUUID().toString()
)