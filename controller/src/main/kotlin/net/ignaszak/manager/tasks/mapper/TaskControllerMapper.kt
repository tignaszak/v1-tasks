package net.ignaszak.manager.tasks.mapper

import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import net.ignaszak.manager.tasks.rest.response.TaskResponse
import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO
import net.ignaszak.manager.tasks.to.task.TaskTO
import org.mapstruct.Mapper
import org.springframework.stereotype.Component
import java.util.Collections.unmodifiableSet
import java.util.stream.Collectors
import kotlin.collections.LinkedHashSet

@Component
@Mapper(componentModel = "spring")
interface TaskControllerMapper {
    fun createUpdateTaskRequestToCreateUpdateTaskTO(createUpdateTaskRequest: CreateUpdateTaskRequest): CreateUpdateTaskTO
    fun taskTOToTaskResponse(taskTO: TaskTO): TaskResponse

    fun taskTOSetToTaskResponseSet(taskTOSet: Set<TaskTO>): Set<TaskResponse> {
        val memberResponseSet = taskTOSet.stream()
            .map(this::taskTOToTaskResponse)
            .collect(Collectors.toCollection {
                LinkedHashSet()
            })

        return unmodifiableSet(memberResponseSet)
    }
}