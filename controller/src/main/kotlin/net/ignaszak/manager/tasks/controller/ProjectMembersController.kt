package net.ignaszak.manager.tasks.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import net.ignaszak.manager.commons.openapi.annotations.ApiNoContentResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiOKResponse
import net.ignaszak.manager.commons.openapi.annotations.UserJwtSecurityRequirement
import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.filtering.annotation.ParameterFilter
import net.ignaszak.manager.commons.user.annotation.UserPrincipal
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import net.ignaszak.manager.tasks.facade.ServiceFacade
import net.ignaszak.manager.tasks.openapi.ApiMemberPublicIdParameter
import net.ignaszak.manager.tasks.openapi.ApiProjectMemberFilterParameter
import net.ignaszak.manager.tasks.openapi.ApiProjectPublicIdParameter
import net.ignaszak.manager.tasks.rest.filter.ProjectMemberFilterRequest
import net.ignaszak.manager.tasks.rest.request.CreateMemberRequest
import net.ignaszak.manager.tasks.rest.request.UpdateMemberRequest
import org.springdoc.api.annotations.ParameterObject
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/projects/{publicId}/members")
@Tag(name = "Project members", description = "Project members operations")
class ProjectMembersController(private val serviceFacade: ServiceFacade) {

    @GetMapping
    @Operation(summary = "Get project members")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun getMembers(
        @ApiProjectPublicIdParameter @PathVariable publicId: String,
        @ParameterObject pageRequest: PageRequest,
        @ApiProjectMemberFilterParameter @ParameterFilter filter: ProjectMemberFilterRequest,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.getProjectMembers(publicId, pageRequest, filter, userPrincipalTO.publicId)

    @PostMapping
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Add member to project")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun addMember(
        @ApiProjectPublicIdParameter @PathVariable publicId: String,
        @RequestBody request: CreateMemberRequest,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.addMemberToProject(publicId, request, userPrincipalTO.publicId)

    @PutMapping("/{memberPublicId}")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Update project member")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun updateMember(
        @ApiProjectPublicIdParameter @PathVariable("publicId") projectPublicId: String,
        @ApiMemberPublicIdParameter @PathVariable memberPublicId: String,
        @RequestBody request: UpdateMemberRequest,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.updateMember(projectPublicId, memberPublicId, request, userPrincipalTO.publicId)

    @DeleteMapping("/{memberPublicId}")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Delete project member")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun deleteMember(
        @ApiProjectPublicIdParameter @PathVariable("publicId") projectPublicId: String,
        @ApiMemberPublicIdParameter @PathVariable memberPublicId: String,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.deleteMember(projectPublicId, memberPublicId, userPrincipalTO.publicId)

    @PostMapping("/join")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Join logged user to project as member")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun join(
        @ApiProjectPublicIdParameter @PathVariable publicId: String,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.joinMemberToProject(publicId, userPrincipalTO.publicId)

    @DeleteMapping("/leave")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Leave logged user from project")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun leave(
        @ApiProjectPublicIdParameter @PathVariable publicId: String,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.leaveProject(publicId, userPrincipalTO.publicId)
}