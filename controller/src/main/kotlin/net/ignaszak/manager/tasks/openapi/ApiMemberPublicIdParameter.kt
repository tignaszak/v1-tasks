package net.ignaszak.manager.tasks.openapi

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn

@Parameter(description = "Project member public id", required = true, `in` = ParameterIn.PATH, example = "042ce9ed-25c2-4ce6-8add-ac6ee62b2add")
annotation class ApiMemberPublicIdParameter