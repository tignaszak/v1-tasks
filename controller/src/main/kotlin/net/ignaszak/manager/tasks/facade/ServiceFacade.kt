package net.ignaszak.manager.tasks.facade

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.tasks.rest.filter.ProjectFilterRequest
import net.ignaszak.manager.tasks.rest.filter.ProjectMemberFilterRequest
import net.ignaszak.manager.tasks.rest.filter.TaskFilterRequest
import net.ignaszak.manager.tasks.rest.request.CreateMemberRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateProjectRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import net.ignaszak.manager.tasks.rest.request.UpdateMemberRequest
import net.ignaszak.manager.tasks.rest.response.MemberResponse
import net.ignaszak.manager.tasks.rest.response.ProjectResponse
import net.ignaszak.manager.tasks.rest.response.TaskResponse

interface ServiceFacade {
    fun createTask(request: CreateUpdateTaskRequest, userPublicId: String): DataResponse<TaskResponse>
    fun updateTask(publicId: String, request: CreateUpdateTaskRequest, userPublicId: String): DataResponse<TaskResponse>
    fun getTaskById(publicId: String, userPublicId: String): DataResponse<TaskResponse>
    fun deleteTask(publicId: String, userPublicId: String)
    fun getTasks(pageRequest: PageRequest, filter: TaskFilterRequest): ListResponse<TaskResponse>

    fun createProject(request: CreateUpdateProjectRequest, userPublicId: String) : DataResponse<ProjectResponse>
    fun updateProject(publicId: String, request: CreateUpdateProjectRequest) : DataResponse<ProjectResponse>
    fun getProjects(pageRequest: PageRequest, filter: ProjectFilterRequest) : ListResponse<ProjectResponse>
    fun getProjectById(publicId: String) : DataResponse<ProjectResponse>
    fun deleteProject(publicId: String)

    fun createProjectTask(publicId: String, request: CreateUpdateTaskRequest, userPublicId: String) : DataResponse<TaskResponse>
    fun attachTaskToProject(projectPublicId: String, taskPublicId: String, userPublicId: String)
    fun detachTaskFromProject(projectPublicId: String, taskPublicId: String, userPublicId: String)
    fun getProjectTasks(publicId: String, pageRequest: PageRequest, filter: TaskFilterRequest): ListResponse<TaskResponse>

    fun getProjectMembers(projectPublicId: String, pageRequest: PageRequest, filter: ProjectMemberFilterRequest, userPublicId: String): ListResponse<MemberResponse>
    fun addMemberToProject(projectPublicId: String, request: CreateMemberRequest, userPublicId: String)
    fun updateMember(projectPublicId: String, memberPublicId: String, request: UpdateMemberRequest, userPublicId: String)
    fun deleteMember(projectPublicId: String, memberPublicId: String, userPublicId: String)
    fun joinMemberToProject(projectPublicId: String, userPublicId: String)
    fun leaveProject(projectPublicId: String, userPublicId: String)
}