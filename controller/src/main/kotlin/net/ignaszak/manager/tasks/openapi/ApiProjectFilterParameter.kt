package net.ignaszak.manager.tasks.openapi

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.Schema
import net.ignaszak.manager.tasks.openapi.Description.PROJECT_FILTER_DESCRIPTION

@Parameter(
    schema = Schema(implementation = String::class),
    description = PROJECT_FILTER_DESCRIPTION,
)
annotation class ApiProjectFilterParameter
