package net.ignaszak.manager.tasks

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import net.ignaszak.manager.commons.openapi.description.AppDescription.CRITERIA_DESCRIPTION
import net.ignaszak.manager.commons.spring.config.CommonOpenApiConfig
import net.ignaszak.manager.commons.spring.config.CommonWebSecurityConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Import

@SpringBootApplication
@EnableEurekaClient
@Import(value = [CommonWebSecurityConfig::class, CommonOpenApiConfig::class])
@OpenAPIDefinition(info = Info(title = "Tasks API", version = "1.0", description = CRITERIA_DESCRIPTION))
open class TasksApplication

fun main(args: Array<String>) {
    runApplication<TasksApplication>(*args)
}
