package net.ignaszak.manager.tasks.mapper

import net.ignaszak.manager.tasks.rest.request.CreateUpdateProjectRequest
import net.ignaszak.manager.tasks.rest.response.ProjectResponse
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import net.ignaszak.manager.tasks.to.project.ProjectTO
import org.mapstruct.Mapper
import org.springframework.stereotype.Component
import java.util.Collections.unmodifiableSet
import java.util.stream.Collectors

@Component
@Mapper(componentModel = "spring")
interface ProjectControllerMapper {
    fun createUpdateProjectRequestToCreateUpdateProjectTO(request: CreateUpdateProjectRequest): CreateUpdateProjectTO
    fun projectTOToProjectResponse(projectTO: ProjectTO): ProjectResponse

    fun projectTOSetToProjectResponseSet(projectTOSet: Set<ProjectTO>): Set<ProjectResponse> {
        val projectResponseSet = projectTOSet.stream()
            .map(this::projectTOToProjectResponse)
            .collect(Collectors.toCollection {
                LinkedHashSet()
            })

        return unmodifiableSet(projectResponseSet)
    }
}