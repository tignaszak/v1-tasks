package net.ignaszak.manager.tasks.mapper

import net.ignaszak.manager.tasks.rest.request.CreateMemberRequest
import net.ignaszak.manager.tasks.rest.request.UpdateMemberRequest
import net.ignaszak.manager.tasks.rest.response.MemberResponse
import net.ignaszak.manager.tasks.to.project.member.MemberTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role
import org.springframework.stereotype.Component
import java.util.Collections
import java.util.stream.Collectors

@Component
open class ProjectMemberControllerMapper {
    fun memberTOSetToMemberResponseSet(memberTOSet: Set<MemberTO>): Set<MemberResponse> {
        val memberResponseSet = memberTOSet.stream()
            .map {
                MemberResponse(it.publicId, it.role.name)
            }
            .collect(Collectors.toCollection {
                LinkedHashSet()
            })

        return Collections.unmodifiableSet(memberResponseSet)
    }

    fun createMemberRequestToMemberTO(request: CreateMemberRequest): MemberTO {
        return MemberTO(request.publicId, getMemberRole(request.role))
    }

    fun updateMemberRequestToMemberTO(request: UpdateMemberRequest, memberPublicId: String): MemberTO {
        return MemberTO(memberPublicId, getMemberRole(request.role))
    }

    private fun getMemberRole(role: String): Role {
        return try {
            Role.valueOf(role)
        } catch (e: IllegalArgumentException) {
            Role.MEMBER
        }
    }
}