package net.ignaszak.manager.tasks.mapper

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.rest.filter.TaskFilterRequest
import net.ignaszak.manager.tasks.to.task.TaskFilterTO
import org.springframework.stereotype.Component
import net.ignaszak.manager.commons.spring.restcriteria.mapper.RestCriteriaMapper as CommonRestCriteriaMapper

@Component
class TaskRestCriteriaMapper(private val restCriteriaMapper: CommonRestCriteriaMapper) : RestCriteriaMapper<TaskFilterRequest, TaskFilterTO> {
    override fun pageRequestAndFilterRequestToCriteriaTO(pageRequest: PageRequest, filterRequest: TaskFilterRequest) =
        CriteriaTO(
            restCriteriaMapper.pageRequestToPageCriteriaTO(pageRequest),
            taskFilterRequestToTaskFilterTO(filterRequest)
        )

    override fun getCommonRestCriteriaMapper() = restCriteriaMapper

    private fun taskFilterRequestToTaskFilterTO(request: TaskFilterRequest) =
        TaskFilterTO(
            restCriteriaMapper.stringOperatorToStringOperatorTO(request.publicId),
            restCriteriaMapper.stringOperatorToStringOperatorTO(request.authorPublicId),
            restCriteriaMapper.stringOperatorToStringOperatorTO(request.projectPublicId),
            restCriteriaMapper.stringOperatorToStringOperatorTO(request.title),
            restCriteriaMapper.stringOperatorToStringOperatorTO(request.text),
            restCriteriaMapper.dateOperatorToDateOperatorTO(request.creationDate),
            restCriteriaMapper.dateOperatorToDateOperatorTO(request.modificationDate)
        )
}