package net.ignaszak.manager.tasks.mapper

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.rest.filter.ProjectFilterRequest
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import org.springframework.stereotype.Component
import net.ignaszak.manager.commons.spring.restcriteria.mapper.RestCriteriaMapper as CommonRestCriteriaMapper

@Component
class ProjectRestCriteriaMapper(private val restCriteriaMapper: CommonRestCriteriaMapper): RestCriteriaMapper<ProjectFilterRequest, ProjectFilterTO> {
    override fun pageRequestAndFilterRequestToCriteriaTO(pageRequest: PageRequest, filterRequest: ProjectFilterRequest) =
        CriteriaTO(
            restCriteriaMapper.pageRequestToPageCriteriaTO(pageRequest),
            projectFilterRequestTOProjectFilterTO(filterRequest)
        )

    override fun getCommonRestCriteriaMapper() = restCriteriaMapper

    private fun projectFilterRequestTOProjectFilterTO(projectFilterRequest: ProjectFilterRequest) =
        ProjectFilterTO(
            restCriteriaMapper.stringOperatorToStringOperatorTO(projectFilterRequest.publicId),
            restCriteriaMapper.stringOperatorToStringOperatorTO(projectFilterRequest.title),
            restCriteriaMapper.dateOperatorToDateOperatorTO(projectFilterRequest.creationDate),
            restCriteriaMapper.dateOperatorToDateOperatorTO(projectFilterRequest.modificationDate)
        )
}