package net.ignaszak.manager.tasks.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import net.ignaszak.manager.commons.openapi.annotations.ApiCreatedResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiNoContentResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiOKResponse
import net.ignaszak.manager.commons.openapi.annotations.UserJwtSecurityRequirement
import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.filtering.annotation.ParameterFilter
import net.ignaszak.manager.commons.user.annotation.UserPrincipal
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import net.ignaszak.manager.tasks.facade.ServiceFacade
import net.ignaszak.manager.tasks.openapi.ApiProjectPublicIdParameter
import net.ignaszak.manager.tasks.openapi.ApiTaskFilterParameter
import net.ignaszak.manager.tasks.openapi.ApiTaskPublicIdParameter
import net.ignaszak.manager.tasks.rest.filter.TaskFilterRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import org.springdoc.api.annotations.ParameterObject
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/projects/{publicId}/tasks")
@Tag(name = "Project tasks", description = "Project tasks operations")
class ProjectTasksController(private val serviceFacade: ServiceFacade) {

    @PostMapping
    @ResponseStatus(CREATED)
    @Operation(summary = "Create task for given project")
    @UserJwtSecurityRequirement
    @ApiCreatedResponse
    fun create(
        @ApiProjectPublicIdParameter @PathVariable publicId: String,
        @RequestBody request: CreateUpdateTaskRequest,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.createProjectTask(publicId, request, userPrincipalTO.publicId)

    @PostMapping("/{taskPublicId}")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Attach task to project")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun attachTask(
        @ApiProjectPublicIdParameter @PathVariable("publicId") projectPublicId: String,
        @ApiTaskPublicIdParameter @PathVariable taskPublicId: String,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.attachTaskToProject(projectPublicId, taskPublicId, userPrincipalTO.publicId)

    @DeleteMapping("/{taskPublicId}")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Detach task from project")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun detachTask(
        @ApiProjectPublicIdParameter @PathVariable("publicId") projectPublicId: String,
        @ApiTaskPublicIdParameter @PathVariable taskPublicId: String,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.detachTaskFromProject(projectPublicId, taskPublicId, userPrincipalTO.publicId)

    @GetMapping
    @Operation(summary = "Get project tasks")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun getProjectTasks(
        @ApiProjectPublicIdParameter @PathVariable publicId: String,
        @ParameterObject pageRequest: PageRequest,
        @ApiTaskFilterParameter @ParameterFilter filter: TaskFilterRequest
    ) = serviceFacade.getProjectTasks(publicId, pageRequest, filter)
}