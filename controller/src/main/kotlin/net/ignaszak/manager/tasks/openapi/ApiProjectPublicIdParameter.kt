package net.ignaszak.manager.tasks.openapi

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn

@Parameter(description = "Project public id", required = true, `in` = ParameterIn.PATH, example = "f75a505e-0a63-464d-9211-fdd17693b00c")
annotation class ApiProjectPublicIdParameter