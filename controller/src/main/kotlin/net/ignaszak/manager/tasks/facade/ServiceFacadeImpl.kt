package net.ignaszak.manager.tasks.facade

import net.ignaszak.manager.commons.rest.filter.Operator.Type.EQUALS
import net.ignaszak.manager.commons.rest.filter.StringOperator
import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.restcriteria.paging.service.PagingService
import net.ignaszak.manager.tasks.mapper.ProjectControllerMapper
import net.ignaszak.manager.tasks.mapper.ProjectMemberControllerMapper
import net.ignaszak.manager.tasks.mapper.RestCriteriaMapper
import net.ignaszak.manager.tasks.mapper.TaskControllerMapper
import net.ignaszak.manager.tasks.rest.filter.ProjectFilterRequest
import net.ignaszak.manager.tasks.rest.filter.ProjectMemberFilterRequest
import net.ignaszak.manager.tasks.rest.filter.TaskFilterRequest
import net.ignaszak.manager.tasks.rest.request.CreateMemberRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateProjectRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import net.ignaszak.manager.tasks.rest.request.UpdateMemberRequest
import net.ignaszak.manager.tasks.rest.response.MemberResponse
import net.ignaszak.manager.tasks.rest.response.ProjectResponse
import net.ignaszak.manager.tasks.rest.response.TaskResponse
import net.ignaszak.manager.tasks.servicecontract.MemberService
import net.ignaszak.manager.tasks.servicecontract.ProjectService
import net.ignaszak.manager.tasks.servicecontract.TaskService
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import net.ignaszak.manager.tasks.to.project.member.MemberFilterTO
import net.ignaszak.manager.tasks.to.task.TaskFilterTO
import org.springframework.stereotype.Component

@Component
class ServiceFacadeImpl(private val taskService: TaskService,
                        private val projectService: ProjectService,
                        private val memberService: MemberService,
                        private val pagingService: PagingService,
                        private val taskControllerMapper: TaskControllerMapper,
                        private val projectControllerMapper: ProjectControllerMapper,
                        private val projectMemberControllerMapper: ProjectMemberControllerMapper,
                        private val taskRestCriteriaMapper: RestCriteriaMapper<TaskFilterRequest, TaskFilterTO>,
                        private val projectRestCriteriaMapper: RestCriteriaMapper<ProjectFilterRequest, ProjectFilterTO>,
                        private val projectMemberRestCriteriaMapper: RestCriteriaMapper<ProjectMemberFilterRequest, MemberFilterTO>
) : ServiceFacade {

    private val commonRestCriteriaMapper = taskRestCriteriaMapper.getCommonRestCriteriaMapper()

    override fun createTask(
        request: CreateUpdateTaskRequest,
        userPublicId: String
    ): DataResponse<TaskResponse> {
        val createUpdateTaskTO = taskControllerMapper.createUpdateTaskRequestToCreateUpdateTaskTO(request)
        val taskTO = taskService.create(createUpdateTaskTO, userPublicId)
        val taskResponse = taskControllerMapper.taskTOToTaskResponse(taskTO)

        return DataResponse(taskResponse)
    }

    override fun updateTask(
        publicId: String,
        request: CreateUpdateTaskRequest,
        userPublicId: String
    ): DataResponse<TaskResponse> {
        val createUpdateTaskTO = taskControllerMapper.createUpdateTaskRequestToCreateUpdateTaskTO(request)
        val taskTO = taskService.update(createUpdateTaskTO, publicId, userPublicId)
        val taskResponse = taskControllerMapper.taskTOToTaskResponse(taskTO)

        return DataResponse(taskResponse)
    }

    override fun getTaskById(publicId: String, userPublicId: String): DataResponse<TaskResponse> {
        val taskTO = taskService.getById(publicId, userPublicId)
        val taskResponse = taskControllerMapper.taskTOToTaskResponse(taskTO)

        return DataResponse(taskResponse)
    }

    override fun deleteTask(publicId: String, userPublicId: String) = taskService.deleteById(publicId, userPublicId)

    override fun getTasks(pageRequest: PageRequest, filter: TaskFilterRequest): ListResponse<TaskResponse> {
        val criteriaTO = taskRestCriteriaMapper.pageRequestAndFilterRequestToCriteriaTO(pageRequest, filter)
        val taskTOSet = taskService.getTasks(criteriaTO)
        val taskResponseSet = taskControllerMapper.taskTOSetToTaskResponseSet(taskTOSet)
        val pageTO = pagingService.buildPageTO(taskService.getTasksCount(criteriaTO), criteriaTO.pageCriteriaTO)
        val pageResponse = commonRestCriteriaMapper.pageTOToPageResponse(pageTO)

        return ListResponse(pageResponse, taskResponseSet)
    }

    override fun createProject(
        request: CreateUpdateProjectRequest,
        userPublicId: String
    ): DataResponse<ProjectResponse> {
        val createUpdateProjectTO = projectControllerMapper.createUpdateProjectRequestToCreateUpdateProjectTO(request)
        val projectTO = projectService.create(createUpdateProjectTO, userPublicId)
        val projectResponse = projectControllerMapper.projectTOToProjectResponse(projectTO)

        return DataResponse(projectResponse)
    }

    override fun updateProject(publicId: String, request: CreateUpdateProjectRequest): DataResponse<ProjectResponse> {
        val createUpdateProjectTO = projectControllerMapper.createUpdateProjectRequestToCreateUpdateProjectTO(request)
        val projectTO = projectService.update(createUpdateProjectTO, publicId)
        val projectResponse = projectControllerMapper.projectTOToProjectResponse(projectTO)

        return DataResponse(projectResponse)
    }

    override fun getProjects(pageRequest: PageRequest, filter: ProjectFilterRequest): ListResponse<ProjectResponse> {
        val criteriaTO = projectRestCriteriaMapper.pageRequestAndFilterRequestToCriteriaTO(pageRequest, filter)
        val projectTOSet = projectService.getProjects(criteriaTO)
        val projectResponseSet = projectControllerMapper.projectTOSetToProjectResponseSet(projectTOSet)
        val pageTO = pagingService.buildPageTO(projectService.getProjectsCount(criteriaTO), criteriaTO.pageCriteriaTO)
        val pageResponse = commonRestCriteriaMapper.pageTOToPageResponse(pageTO)

        return ListResponse(pageResponse, projectResponseSet)
    }

    override fun getProjectById(publicId: String): DataResponse<ProjectResponse> {
        val projectTO = projectService.getById(publicId)
        val projectResponse = projectControllerMapper.projectTOToProjectResponse(projectTO)

        return DataResponse(projectResponse)
    }

    override fun deleteProject(publicId: String) = projectService.deleteById(publicId)

    override fun createProjectTask(
        publicId: String,
        request: CreateUpdateTaskRequest,
        userPublicId: String
    ): DataResponse<TaskResponse> {
        val createUpdateTaskTO = taskControllerMapper.createUpdateTaskRequestToCreateUpdateTaskTO(request)
        val taskTO = taskService.create(createUpdateTaskTO, publicId, userPublicId)
        val taskResponse = taskControllerMapper.taskTOToTaskResponse(taskTO)

        return DataResponse(taskResponse)
    }

    override fun attachTaskToProject(projectPublicId: String, taskPublicId: String, userPublicId: String) =
        projectService.attachTask(projectPublicId, taskPublicId, userPublicId)

    override fun detachTaskFromProject(projectPublicId: String, taskPublicId: String, userPublicId: String) =
        projectService.detachTask(projectPublicId, taskPublicId, userPublicId)

    override fun getProjectTasks(
        publicId: String,
        pageRequest: PageRequest,
        filter: TaskFilterRequest
    ): ListResponse<TaskResponse> {
        projectService.getById(publicId)

        val newFilter = TaskFilterRequest(
            filter.publicId,
            filter.authorPublicId,
            StringOperator(EQUALS, setOf(publicId)),
            filter.title,
            filter.text,
            filter.creationDate,
            filter.modificationDate
        )

        return getTasks(pageRequest, newFilter)
    }

    override fun getProjectMembers(
        projectPublicId: String,
        pageRequest: PageRequest,
        filter: ProjectMemberFilterRequest,
        userPublicId: String
    ): ListResponse<MemberResponse> {
        val criteriaTO = projectMemberRestCriteriaMapper.pageRequestAndFilterRequestToCriteriaTO(pageRequest, filter)
        val memberTOSet = memberService.getMembers(projectPublicId, userPublicId, criteriaTO)
        val memberResponseSet = projectMemberControllerMapper.memberTOSetToMemberResponseSet(memberTOSet)
        val pageTO = pagingService.buildPageTO(memberService.getMembersCount(projectPublicId, criteriaTO), criteriaTO.pageCriteriaTO)
        val pageResponse = commonRestCriteriaMapper.pageTOToPageResponse(pageTO)

        return ListResponse(pageResponse, memberResponseSet)
    }

    override fun addMemberToProject(projectPublicId: String, request: CreateMemberRequest, userPublicId: String) =
        memberService.addMember(
            projectMemberControllerMapper.createMemberRequestToMemberTO(request),
            projectPublicId,
            userPublicId
        )

    override fun updateMember(
        projectPublicId: String,
        memberPublicId: String,
        request: UpdateMemberRequest,
        userPublicId: String
    ) = memberService.updateMember(
        projectMemberControllerMapper.updateMemberRequestToMemberTO(request, memberPublicId),
        projectPublicId,
        userPublicId
    )

    override fun deleteMember(projectPublicId: String, memberPublicId: String, userPublicId: String) =
        memberService.deleteMember(memberPublicId, projectPublicId, userPublicId)

    override fun joinMemberToProject(projectPublicId: String, userPublicId: String) =
        memberService.joinProject(projectPublicId, userPublicId)

    override fun leaveProject(projectPublicId: String, userPublicId: String) =
        memberService.leaveProject(projectPublicId, userPublicId)
}