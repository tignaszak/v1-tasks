package net.ignaszak.manager.tasks.mapper

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.tasks.rest.filter.ProjectMemberFilterRequest
import net.ignaszak.manager.tasks.to.project.member.MemberFilterTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role
import org.springframework.stereotype.Component
import net.ignaszak.manager.commons.spring.restcriteria.mapper.RestCriteriaMapper as CommonRestCriteriaMapper

@Component
class ProjectMemberRestCriteriaMapper(private val restCriteriaMapper: CommonRestCriteriaMapper): RestCriteriaMapper<ProjectMemberFilterRequest, MemberFilterTO> {

    override fun getCommonRestCriteriaMapper() = restCriteriaMapper

    override fun pageRequestAndFilterRequestToCriteriaTO(
        pageRequest: PageRequest,
        filterRequest: ProjectMemberFilterRequest
    ) = CriteriaTO(
        restCriteriaMapper.pageRequestToPageCriteriaTO(pageRequest),
        projectMemberFilterRequestToMemberFilterTO(filterRequest)
    )

    private fun projectMemberFilterRequestToMemberFilterTO(filterRequest: ProjectMemberFilterRequest) = MemberFilterTO(
        restCriteriaMapper.stringOperatorToStringOperatorTO(filterRequest.publicId),
        restCriteriaMapper.stringOperatorToEnumOperatorTO(filterRequest.role, Role::class.java)
    )
}