package net.ignaszak.manager.tasks.mapper

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO

interface RestCriteriaMapper<R, F> {
    fun getCommonRestCriteriaMapper(): net.ignaszak.manager.commons.spring.restcriteria.mapper.RestCriteriaMapper
    fun pageRequestAndFilterRequestToCriteriaTO(pageRequest: PageRequest, filterRequest: R): CriteriaTO<F>
}