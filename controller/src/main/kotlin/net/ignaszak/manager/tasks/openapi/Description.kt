package net.ignaszak.manager.tasks.openapi

import net.ignaszak.manager.commons.openapi.description.CriteriaDescription.DATE_OPERATORS
import net.ignaszak.manager.commons.openapi.description.CriteriaDescription.PUBLIC_ID_OPERATORS
import net.ignaszak.manager.commons.openapi.description.CriteriaDescription.TEXT_OPERATORS

object Description {
    const val TASK_FILTER_DESCRIPTION = "Available parameters:" +
            "\n - <code>publicId</code> - task public id, $PUBLIC_ID_OPERATORS" +
            "\n - <code>authorPublicId</code> - author public id, $PUBLIC_ID_OPERATORS" +
            "\n - <code>projectPublicId</code> - project public id, $PUBLIC_ID_OPERATORS" +
            "\n - <code>title</code> - task title, $TEXT_OPERATORS" +
            "\n - <code>text</code> - task text, $TEXT_OPERATORS" +
            "\n - <code>creationDate</code> - task creation date, $DATE_OPERATORS" +
            "\n - <code>modificationDate</code> - task modification date, $DATE_OPERATORS"

    const val PROJECT_FILTER_DESCRIPTION = "Available parameters:" +
            "\n - <code>publicId</code> - project public id, $PUBLIC_ID_OPERATORS" +
            "\n - <code>title</code> - project title, $TEXT_OPERATORS" +
            "\n - <code>creationDate</code> - project creation date, $DATE_OPERATORS" +
            "\n - <code>modificationDate</code> - project modification date, $DATE_OPERATORS"

    const val PROJECT_TASK_FILTER_DESCRIPTION = "Available parameters:" +
            "\n - <code>publicId</code> - task public id, $PUBLIC_ID_OPERATORS" +
            "\n - <code>authorPublicId</code> - author public id, $PUBLIC_ID_OPERATORS" +
            "\n - <code>title</code> - task title, $TEXT_OPERATORS" +
            "\n - <code>text</code> - task text, $TEXT_OPERATORS" +
            "\n - <code>creationDate</code> - task creation date, $DATE_OPERATORS" +
            "\n - <code>modificationDate</code> - task modification date, $DATE_OPERATORS"

    const val PROJECT_MEMBER_FILTER_DESCRIPTION = "Available parameters:" +
            "\n - <code>publicId</code> - member public id, $PUBLIC_ID_OPERATORS" +
            "\n - <code>role</code> - member role, $PUBLIC_ID_OPERATORS"
}