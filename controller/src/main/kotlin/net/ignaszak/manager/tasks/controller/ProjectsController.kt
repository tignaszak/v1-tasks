package net.ignaszak.manager.tasks.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import net.ignaszak.manager.commons.openapi.annotations.ApiCreatedResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiNoContentResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiOKResponse
import net.ignaszak.manager.commons.openapi.annotations.UserJwtSecurityRequirement
import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.filtering.annotation.ParameterFilter
import net.ignaszak.manager.commons.user.annotation.UserPrincipal
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import net.ignaszak.manager.tasks.facade.ServiceFacade
import net.ignaszak.manager.tasks.openapi.ApiProjectFilterParameter
import net.ignaszak.manager.tasks.openapi.ApiProjectPublicIdParameter
import net.ignaszak.manager.tasks.rest.filter.ProjectFilterRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateProjectRequest
import org.springdoc.api.annotations.ParameterObject
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/projects")
@Tag(name = "Projects", description = "Projects operations")
class ProjectsController(private val serviceFacade: ServiceFacade) {

    @PostMapping
    @ResponseStatus(CREATED)
    @Operation(summary = "Create project")
    @UserJwtSecurityRequirement
    @ApiCreatedResponse
    fun create(
        @RequestBody request: CreateUpdateProjectRequest,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.createProject(request, userPrincipalTO.publicId)

    @PutMapping("/{publicId}")
    @Operation(summary = "Update project")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun update(
        @ApiProjectPublicIdParameter @PathVariable publicId: String,
        @RequestBody request: CreateUpdateProjectRequest
    ) = serviceFacade.updateProject(publicId, request)

    @GetMapping
    @Operation(summary = "Get projects")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun getProjects(
        @ParameterObject pageRequest: PageRequest,
        @ApiProjectFilterParameter @ParameterFilter filter: ProjectFilterRequest
    ) = serviceFacade.getProjects(pageRequest, filter)

    @GetMapping("/{publicId}")
    @Operation(summary = "Get project by public id")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun getById(@ApiProjectPublicIdParameter @PathVariable publicId: String) = serviceFacade.getProjectById(publicId)

    @DeleteMapping("/{publicId}")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Delete project by public id")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun delete(@ApiProjectPublicIdParameter @PathVariable publicId: String) = serviceFacade.deleteProject(publicId)
}