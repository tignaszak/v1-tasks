package net.ignaszak.manager.tasks.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import net.ignaszak.manager.commons.openapi.annotations.ApiCreatedResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiNoContentResponse
import net.ignaszak.manager.commons.openapi.annotations.ApiOKResponse
import net.ignaszak.manager.commons.openapi.annotations.UserJwtSecurityRequirement
import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.restcriteria.filtering.annotation.ParameterFilter
import net.ignaszak.manager.commons.user.annotation.UserPrincipal
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import net.ignaszak.manager.tasks.facade.ServiceFacade
import net.ignaszak.manager.tasks.openapi.ApiTaskFilterParameter
import net.ignaszak.manager.tasks.openapi.ApiTaskPublicIdParameter
import net.ignaszak.manager.tasks.rest.filter.TaskFilterRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import org.springdoc.api.annotations.ParameterObject
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = "Tasks", description = "Task operations")
class TasksController(private val serviceFacade: ServiceFacade) {

    @PostMapping
    @ResponseStatus(CREATED)
    @Operation(summary = "Create task")
    @UserJwtSecurityRequirement
    @ApiCreatedResponse
    fun create(
        @RequestBody request: CreateUpdateTaskRequest,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.createTask(request, userPrincipalTO.publicId)

    @PutMapping("/{publicId}")
    @Operation(summary = "Update task")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun update(
        @ApiTaskPublicIdParameter @PathVariable publicId: String,
        @RequestBody request: CreateUpdateTaskRequest,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.updateTask(publicId, request, userPrincipalTO.publicId)

    @GetMapping("/{publicId}")
    @Operation(summary = "Get task by public id")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun getById(
        @ApiTaskPublicIdParameter @PathVariable publicId: String,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.getTaskById(publicId, userPrincipalTO.publicId)

    @DeleteMapping("/{publicId}")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Delete task by public id")
    @UserJwtSecurityRequirement
    @ApiNoContentResponse
    fun delete(
        @ApiTaskPublicIdParameter @PathVariable publicId: String,
        @UserPrincipal userPrincipalTO: UserPrincipalTO
    ) = serviceFacade.deleteTask(publicId, userPrincipalTO.publicId)

    @GetMapping
    @Operation(summary = "Get tasks")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    fun getTasks(
        @ParameterObject pageRequest: PageRequest,
        @ApiTaskFilterParameter @ParameterFilter filter: TaskFilterRequest
    ) = serviceFacade.getTasks(pageRequest, filter)
}