package net.ignaszak.manager.tasks.config

import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.PathItem
import io.swagger.v3.oas.models.media.StringSchema
import io.swagger.v3.oas.models.parameters.Parameter
import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.openapi.OpenApiUtils.getFilterParameter
import net.ignaszak.manager.commons.openapi.OpenApiUtils.getOKListResponse
import net.ignaszak.manager.commons.openapi.OpenApiUtils.getPageParameter
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.tasks.openapi.Description.PROJECT_FILTER_DESCRIPTION
import net.ignaszak.manager.tasks.openapi.Description.PROJECT_MEMBER_FILTER_DESCRIPTION
import net.ignaszak.manager.tasks.openapi.Description.PROJECT_TASK_FILTER_DESCRIPTION
import net.ignaszak.manager.tasks.openapi.Description.TASK_FILTER_DESCRIPTION
import net.ignaszak.manager.tasks.rest.response.MemberResponse
import net.ignaszak.manager.tasks.rest.response.ProjectResponse
import net.ignaszak.manager.tasks.rest.response.TaskResponse
import org.springdoc.core.customizers.OpenApiCustomiser
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy

private const val STRING = "string"

@Configuration
open class OpenApiConfig(private val managerProperties: ManagerProperties, private val dateProvider: IDateTimeProvider) {

    @Bean
    @Lazy(false)
    open fun endpointCustomizer() = OpenApiCustomiser {
            it.paths
                .addPathItem(
                    managerProperties.server.pathPrefix,
                    getTasksPath("Get tasks", "Tasks", TASK_FILTER_DESCRIPTION)
                )
                .addPathItem(managerProperties.server.pathPrefix + "/projects", getProjectsPath())
                .addPathItem(
                    managerProperties.server.pathPrefix + "/projects/{publicId}/tasks",
                    getTasksPath("Get project tasks", "Project tasks", PROJECT_TASK_FILTER_DESCRIPTION, projectPublicIdPathParameter())
                )
                .addPathItem(
                    managerProperties.server.pathPrefix + "/projects/{publicId}/members",
                    getMembersPath()
                )
        }

    private fun getTasksPath(summary: String, tag: String, filterDescription: String, vararg parameters: Parameter): PathItem {
        val operation = Operation()
        operation.responses(getOKListResponse(getTaskResponse()))
        operation.summary = summary
        operation.addTagsItem(tag)

        parameters.forEach(operation::addParametersItem)

        operation.addParametersItem(getPageParameter())
        operation.addParametersItem(getFilterParameter(filterDescription))

        return PathItem()[operation]
    }

    private fun getMembersPath(): PathItem {
        val operation = Operation()
        operation.responses(getOKListResponse(getMembersResponse()))
        operation.summary = "Get project members"
        operation.addTagsItem("Project members")

        operation.parameters = listOf(
            projectPublicIdPathParameter(),
            getPageParameter(),
            getFilterParameter(PROJECT_MEMBER_FILTER_DESCRIPTION)
        )

        return PathItem()[operation]
    }

    private fun projectPublicIdPathParameter() = Parameter()
        .`in`("path")
        .schema(StringSchema())
        .name("publicId")
        .description("Project public id")
        .example("f75a505e-0a63-464d-9211-fdd17693b00c")

    private fun getTaskResponse() = TaskResponse(STRING, STRING, STRING, STRING, dateProvider.getNow(), dateProvider.getNow())

    private fun getMembersResponse() = MemberResponse(STRING, STRING)

    private fun getProjectsPath(): PathItem {
        val operation = Operation()
        operation.responses(getOKListResponse(getProjectResponse()))
        operation.summary = "Get projects"
        operation.addTagsItem("Projects")
        operation.parameters(
            listOf(
                getPageParameter(),
                getFilterParameter(PROJECT_FILTER_DESCRIPTION)
            )
        )

        return PathItem()[operation]
    }

    private fun getProjectResponse() = ProjectResponse(STRING, STRING, dateProvider.getNow(), dateProvider.getNow())
}