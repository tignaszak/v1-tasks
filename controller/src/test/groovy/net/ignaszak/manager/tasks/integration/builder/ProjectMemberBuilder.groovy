package net.ignaszak.manager.tasks.integration.builder

import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity

class ProjectMemberBuilder {

    String userPublicId = UserHelper.ANY_PUBLIC_ID
    String userRole = ProjectMemberEntity.UserRole.MEMBER.name()
    Boolean isProjectCreator = false
    Integer projectId = 1

    static ProjectMemberBuilder "new"() {
        new ProjectMemberBuilder()
    }

    private ProjectMemberBuilder() {}

    ProjectMemberBuilder setUserPublicId(final String userPublicId) {
        this.userPublicId = userPublicId
        this
    }

    ProjectMemberBuilder setUserRole(final ProjectMemberEntity.UserRole userRole) {
        this.userRole = userRole.name()
        this
    }

    ProjectMemberBuilder setIsProjectCreator(final Boolean isProjectCreator) {
        this.isProjectCreator = isProjectCreator
        this
    }

    ProjectMemberBuilder setProjectId(final Integer projectId) {
        this.projectId = projectId
        this
    }
}
