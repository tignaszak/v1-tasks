package net.ignaszak.manager.tasks

import java.time.LocalDateTime

final class TestConstants {

    public static final LocalDateTime ANY_DATE = LocalDateTime.now().withNano(0)

    public static final String ANY_TASK_PUBLIC_ID = "2f2d7da6-69b4-11ec-90d6-0242ac120003"
    public static final String ANY_OTHER_TASK_PUBLIC_ID = "dc393b59-9033-443d-9aec-56d48703b76a"
    public static final String ANY_TASK_TEXT = "taskText"
    public static final String ANY_TASK_TITLE = "taskTitle"

    public static final String ANY_OTHER_USER_PUBLIC_ID = "22f72b1b-d5ce-4206-b054-31e76e10c53b"

    public static final String ANY_OTHER_PROJECT_PUBLIC_ID = "6e4586fd-a386-4091-9255-910f5bb58747"
    public static final String ANY_PROJECT_PUBLIC_ID = "3b2c3c3b-1019-4415-ac30-6cb2150821af"
    public static final String ANY_PROJECT_TITLE = "projectTitle"

    private TestConstants() {}
}
