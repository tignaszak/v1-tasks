package net.ignaszak.manager.tasks.integration.builder

import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.tasks.integration.DBHelper

import java.time.LocalDateTime

import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_TEXT
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_TITLE

final class TaskBuilder {

    static TaskBuilder "new"() {
        new TaskBuilder()
    }

    private TaskBuilder() {}

    String publicId = ANY_TASK_PUBLIC_ID
    String authorPublicId = UserHelper.ANY_PUBLIC_ID
    String title = ANY_TASK_TITLE
    String text = ANY_TASK_TEXT
    LocalDateTime creationDate = DBHelper.NOW
    LocalDateTime modificationDate = null
    Integer projectId = null

    TaskBuilder setPublicId(final String publicId) {
        this.publicId = publicId
        return this
    }

    TaskBuilder setAuthorPublicId(final String authorPublicId) {
        this.authorPublicId = authorPublicId
        return this
    }

    TaskBuilder setTitle(final String title) {
        this.title = title
        return this
    }

    TaskBuilder setText(final String text) {
        this.text = text
        return this
    }

    TaskBuilder setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate
        return this
    }

    TaskBuilder setModificationDate(final LocalDateTime modificationDate) {
        this.modificationDate = modificationDate
        return this
    }

    TaskBuilder setProjectId(final Integer projectId) {
        this.projectId = projectId
        return this
    }
}
