package net.ignaszak.manager.tasks.controller

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import net.ignaszak.manager.tasks.facade.ServiceFacade
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType

import java.nio.charset.StandardCharsets

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.TestUtils.assertResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestHelper.anyCreateUpdateProjectRequest
import static net.ignaszak.manager.tasks.TestHelper.anyCreateUpdateProjectTO
import static net.ignaszak.manager.tasks.TestHelper.anyPageResponse
import static net.ignaszak.manager.tasks.TestHelper.anyProjectResponse
import static net.ignaszak.manager.tasks.TestHelper.anyProjectResponseSet
import static net.ignaszak.manager.tasks.TestHelper.expectProjectResponse
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(ProjectsController)
class ProjectsControllerSpec extends ControllerSpecification {

    @SpringBean
    private ServiceFacade serviceFacadeMock = Mock()

    def "create new project"() {
        given:
        1 * serviceFacadeMock.createProject(anyCreateUpdateProjectRequest(), UserHelper.ANY_PUBLIC_ID) >> new DataResponse(anyProjectResponse())

        when:
        def request = post("/api/projects")
                .header(TOKEN_HEADER, anyToken())
                .content(toJson(anyCreateUpdateProjectRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        expectProjectResponse(mvc.perform(request), status().isCreated())
    }

    def "update project by public id"() {
        given:
        1 * serviceFacadeMock.updateProject(ANY_PROJECT_PUBLIC_ID, anyCreateUpdateProjectRequest()) >> new DataResponse(anyProjectResponse())

        when:
        def request = put("/api/projects/{publicId}", ANY_PROJECT_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())
                .content(toJson(anyCreateUpdateProjectTO()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        expectProjectResponse(mvc.perform(request), status().isOk())
    }

    def "get project by public id"() {
        given:
        1 * serviceFacadeMock.getProjectById(ANY_PROJECT_PUBLIC_ID) >> new DataResponse(anyProjectResponse())

        when:
        def request = get("/api/projects/{publicId}", ANY_PROJECT_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        expectProjectResponse(mvc.perform(request), status().isOk())
    }

    def "get all projects"() {
        given:
        1 * serviceFacadeMock.getProjects(
                {
                    PageRequest pageRequest ->
                        pageRequest.page == 1
                },
                _
        ) >> new ListResponse(anyPageResponse(), anyProjectResponseSet())

        when:
        def request = get("/api/projects?page=1").header(TOKEN_HEADER, anyToken())

        then:
        assertResponse(mvc.perform(request), status().isOk(), new ListResponse(anyPageResponse(), anyProjectResponseSet()))
    }

    def "remove project by public id"() {
        when:
        def request = delete("/api/projects/{publicId}", ANY_PROJECT_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        mvc.perform(request).andExpect(status().isNoContent())
    }
}
