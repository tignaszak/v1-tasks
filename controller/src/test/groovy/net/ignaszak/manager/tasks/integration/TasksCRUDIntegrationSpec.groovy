package net.ignaszak.manager.tasks.integration

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.commons.test.specification.IntegrationSpecification
import net.ignaszak.manager.tasks.integration.builder.ProjectBuilder
import net.ignaszak.manager.tasks.integration.builder.TaskBuilder
import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import org.springframework.http.ResponseEntity
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared

import java.sql.Connection
import java.time.LocalDateTime

import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN

import static net.ignaszak.manager.commons.test.TestUtils.assertOnePageSize
import static net.ignaszak.manager.tasks.TestConstants.ANY_OTHER_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_OTHER_USER_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_TEXT
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_TITLE
import static net.ignaszak.manager.tasks.TestHelper.assertDate
import static net.ignaszak.manager.tasks.integration.DBHelper.NOW
import static net.ignaszak.manager.tasks.integration.DBHelper.count
import static net.ignaszak.manager.tasks.integration.DBHelper.countByPublicId
import static net.ignaszak.manager.tasks.integration.DBHelper.selectByPublicId
import static net.ignaszak.manager.tasks.service.task.error.TaskError.TSK_INVALID_TITLE
import static net.ignaszak.manager.tasks.service.task.error.TaskError.TSK_NOT_FOUND
import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.FORBIDDEN
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

class TasksCRUDIntegrationSpec extends IntegrationSpecification {

    @Shared
    private static final PostgreSQLContainer POSTGRESQL_CONTAINER = new PostgreSQLContainer("postgres:15.0")
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa")

    private static Connection CONNECTION
    private static DBHelper DB_HELPER

    def cleanup() {
        DB_HELPER.clearTask()
    }

    def cleanupSpec() {
        POSTGRESQL_CONTAINER.stop()
    }

    @DynamicPropertySource
    private static void postgresqlProperties(DynamicPropertyRegistry registry) {
        POSTGRESQL_CONTAINER.start()

        registry.add("spring.datasource.url", POSTGRESQL_CONTAINER::getJdbcUrl)
        registry.add("spring.datasource.username", POSTGRESQL_CONTAINER::getUsername)
        registry.add("spring.datasource.password", POSTGRESQL_CONTAINER::getPassword)

        HikariConfig hikariConfig = new HikariConfig()
        hikariConfig.setJdbcUrl(POSTGRESQL_CONTAINER.getJdbcUrl())
        hikariConfig.setUsername(POSTGRESQL_CONTAINER.getUsername())
        hikariConfig.setPassword(POSTGRESQL_CONTAINER.getPassword())
        CONNECTION = new HikariDataSource(hikariConfig).getConnection()
        DB_HELPER = new DBHelper(CONNECTION)
    }

    def "create task" () {
        given:
        def request = new CreateUpdateTaskRequest(ANY_TASK_TITLE, ANY_TASK_TEXT)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == CREATED
        !responseEntity.body.data.publicId.isBlank()
        responseEntity.body.data.authorPublicId == UserHelper.ANY_PUBLIC_ID
        responseEntity.body.data.title == ANY_TASK_TITLE
        responseEntity.body.data.text == ANY_TASK_TEXT
        responseEntity.body.data.creationDate != null
        responseEntity.body.data.modificationDate == null

        and:
        def stmt = CONNECTION.prepareStatement(countByPublicId("task"))
        stmt.setString(1, responseEntity.body.data.publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getInt(1) == 1
    }

    def "error while creating task with incorrect data"() {
        given:
        def request = new CreateUpdateTaskRequest("", ANY_TASK_TEXT)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TSK_INVALID_TITLE)
    }

    def "get task by public id"() {
        setup:
        DB_HELPER.insert(TaskBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/${ANY_TASK_PUBLIC_ID}") as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.data.publicId == ANY_TASK_PUBLIC_ID
        responseEntity.body.data.authorPublicId == UserHelper.ANY_PUBLIC_ID
        responseEntity.body.data.title == ANY_TASK_TITLE
        responseEntity.body.data.text == ANY_TASK_TEXT
        assertDate(NOW, responseEntity.body.data.creationDate)
        responseEntity.body.data.modificationDate == null
    }

    def "return not found while getting task by public id"() {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, TSK_NOT_FOUND)
    }

    def "update task by public id"() {
        given:
        DB_HELPER.insert(TaskBuilder.new())
        def newTaskTitle = "newTaskTitle"
        def newTaskText = "newTaskText"
        def request = new CreateUpdateTaskRequest(newTaskTitle, newTaskText)

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/${ANY_TASK_PUBLIC_ID}", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.data.publicId == ANY_TASK_PUBLIC_ID
        responseEntity.body.data.authorPublicId == UserHelper.ANY_PUBLIC_ID
        responseEntity.body.data.title == newTaskTitle
        responseEntity.body.data.text == newTaskText
        assertDate(NOW, responseEntity.body.data.creationDate)
        responseEntity.body.data.modificationDate != null

        and:
        def stmt = CONNECTION.prepareStatement(selectByPublicId("task"))
        stmt.setString(1, responseEntity.body.data.publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getString("title") == newTaskTitle
        resultSet.getString("text") == newTaskText
    }

    def "return error while updating task with invalid data"() {
        given:
        DB_HELPER.insert(TaskBuilder.new())
        def request = new CreateUpdateTaskRequest("", ANY_TASK_TEXT)

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/${ANY_TASK_PUBLIC_ID}", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TSK_INVALID_TITLE)
    }

    def "return error while updating non existing task"() {
        given:
        def request = new CreateUpdateTaskRequest(ANY_TASK_TITLE, ANY_TASK_TEXT)

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/${ANY_TASK_PUBLIC_ID}", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, TSK_NOT_FOUND)
    }

    def "delete task by public id"() {
        setup:
        DB_HELPER.insert(TaskBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/${ANY_TASK_PUBLIC_ID}")

        then:
        responseEntity.statusCode == NO_CONTENT

        and:
        def resultSet = CONNECTION.prepareStatement(count("task")).executeQuery()
        resultSet.next()
        resultSet.getInt(1) == 0
    }

    def "return error while deleting non existing task"() {
        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, TSK_NOT_FOUND)
    }

    def "return forbidden with unauthenticated request"() {
        when:
        def responseEntity = restTemplateFacade.get("/api/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, AUT_INVALID_TOKEN)
    }

    def "get all tasks"() {
        setup:
        DB_HELPER.insert(TaskBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.get("/api") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        assertPaging(responseEntity)
        responseEntity.statusCode == OK
        responseEntity.body.list != null
        responseEntity.body.list.size() == 1
        responseEntity.body.list[0].publicId == ANY_TASK_PUBLIC_ID
        responseEntity.body.list[0].authorPublicId == UserHelper.ANY_PUBLIC_ID
        responseEntity.body.list[0].title == ANY_TASK_TITLE
        responseEntity.body.list[0].text == ANY_TASK_TEXT
        assertDate(NOW, responseEntity.body.list[0].creationDate)
        responseEntity.body.list[0].modificationDate == null
    }

    def "paginate tasks"(int page, int elements) {
        setup:
        int counter = 0
        List<String> publicIds = new ArrayList<>()
        while (counter++ < 46) {
            def publicId = UUID.randomUUID().toString()

            DB_HELPER.insert(
                    TaskBuilder.new().setPublicId(publicId)
            )

            publicIds.add(publicId)
        }

        when:
        def responseEntity = restTemplateAuthFacade.get("/api?page=${page}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        pagingAssertion(responseEntity)
            .withNumber(page)
            .withTotalPages(3)
            .withTotalElements(46)
            .isValid()
        responseEntity.statusCode == OK
        responseEntity.body.list != null
        responseEntity.body.list.size() == elements
        int i = 0
        for (; i < elements; i++) {
            responseEntity.body.list[i].publicId == publicIds.get( ((page - 1) * 20) + i )
        }
        i == elements

        where:
        page | elements
        1    | 20
        2    | 20
        3    | 6
    }

    @SuppressWarnings("all")
    def "filter tasks"(String filter, TaskFilterPredicate predicate) {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        def tasks = List.of(
                TaskBuilder.new().setAuthorPublicId(ANY_OTHER_USER_PUBLIC_ID).setProjectId(1),
                TaskBuilder.new().setPublicId(ANY_OTHER_TASK_PUBLIC_ID),
                TaskBuilder.new().setPublicId(UUID.randomUUID().toString()).setTitle("test task title test"),
                TaskBuilder.new().setPublicId(UUID.randomUUID().toString()).setTitle("task title"),
                TaskBuilder.new().setPublicId(UUID.randomUUID().toString()).setText("test task text test"),
                TaskBuilder.new().setPublicId(UUID.randomUUID().toString()).setText("task text"),
                TaskBuilder.new().setPublicId(UUID.randomUUID().toString()).setCreationDate(LocalDateTime.of(2022, 04, 01, 0, 1)).setModificationDate(LocalDateTime.of(2022, 04, 01, 0, 1)),
                TaskBuilder.new().setPublicId(UUID.randomUUID().toString()).setCreationDate(LocalDateTime.of(2022, 04, 02, 0, 1)).setModificationDate(LocalDateTime.of(2022, 04, 02, 0, 1)),
                TaskBuilder.new().setPublicId(UUID.randomUUID().toString()).setCreationDate(LocalDateTime.of(2022, 04, 03, 0, 1)).setModificationDate(LocalDateTime.of(2022, 04, 03, 0, 1)),
        )
        tasks.forEach(DB_HELPER::insert)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api?filter=${filter}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        predicate.test(responseEntity, tasks)

        cleanup:
        DB_HELPER.clearTask()
        DB_HELPER.clearProject()

        where:
        filter                                                                       | predicate
        ""                                                                           | (entity, list) -> assertOnePageSize(entity, list.size())
        "publicId:${ANY_OTHER_TASK_PUBLIC_ID}"                                       | (entity, list) -> assertOnePageSize(entity, 1)
        "publicId>${ANY_OTHER_TASK_PUBLIC_ID}"                                       | (entity, list) -> assertOnePageSize(entity, list.size())
        "publicId:[${ANY_OTHER_TASK_PUBLIC_ID},${ANY_TASK_PUBLIC_ID}]"               | (entity, list) -> assertOnePageSize(entity, 2)
        "publicId!:${ANY_OTHER_TASK_PUBLIC_ID}"                                      | (entity, list) -> assertOnePageSize(entity, list.size() - 1)
        "authorPublicId:${ANY_OTHER_USER_PUBLIC_ID}"                                 | (entity, list) -> assertOnePageSize(entity, 1)
        "authorPublicId!:${ANY_OTHER_USER_PUBLIC_ID}"                                | (entity, list) -> assertOnePageSize(entity, list.size() - 1)
        "publicId:${ANY_TASK_PUBLIC_ID}|authorPublicId!:${ANY_OTHER_USER_PUBLIC_ID}" | (entity, list) -> assertOnePageSize(entity, 0)
        "title:task Title"                                                           | (entity, list) -> assertOnePageSize(entity, 1)
        "title>:task Title"                                                          | (entity, list) -> assertOnePageSize(entity, list.size())
        "title:[task Title,test task Title test]"                                    | (entity, list) -> assertOnePageSize(entity, 2)
        "title!:[task Title,test task Title test]"                                   | (entity, list) -> assertOnePageSize(entity, list.size() - 2)
        "title!:${ANY_TASK_TITLE}"                                                   | (entity, list) -> assertOnePageSize(entity, 2)
        "title::task Title"                                                          | (entity, list) -> assertOnePageSize(entity, 2)
        "title!::task Title"                                                         | (entity, list) -> assertOnePageSize(entity, list.size() - 2)
        "title::[task Title]"                                                        | (entity, list) -> assertOnePageSize(entity, 2)
        "title!::[task Title]"                                                       | (entity, list) -> assertOnePageSize(entity, list.size() - 2)
        "text:task Text"                                                             | (entity, list) -> assertOnePageSize(entity, 1)
        "text>:task Text"                                                            | (entity, list) -> assertOnePageSize(entity, list.size())
        "text:[task Text,test task Text test]"                                       | (entity, list) -> assertOnePageSize(entity, 2)
        "text!:[task Text,test task Text test]"                                      | (entity, list) -> assertOnePageSize(entity, list.size() - 2)
        "text!:${ANY_TASK_TEXT}"                                                     | (entity, list) -> assertOnePageSize(entity, 2)
        "text::task Text"                                                            | (entity, list) -> assertOnePageSize(entity, 2)
        "text!::task Text"                                                           | (entity, list) -> assertOnePageSize(entity, list.size() - 2)
        "text::[task Text]"                                                          | (entity, list) -> assertOnePageSize(entity, 2)
        "text!::[task Text]"                                                         | (entity, list) -> assertOnePageSize(entity, list.size() - 2)
        "creationDate:[2022-04-01T00:01,2022-04-03T00:01]"                           | (entity, list) -> assertOnePageSize(entity, 2)
        "creationDate!:[2022-04-01,2022-04-03]"                                      | (entity, list) -> assertOnePageSize(entity, list.size() - 2)
        "creationDate!:2022-04-01"                                                   | (entity, list) -> assertOnePageSize(entity, list.size() - 1)
        "creationDate>2022-04-01<2022-04-03"                                         | (entity, list) -> assertOnePageSize(entity, 1)
        "creationDate>:2022-04-01<:2022-04-03"                                       | (entity, list) -> assertOnePageSize(entity, 3)
        "modificationDate:[2022-04-01,2022-04-03]"                                   | (entity, list) -> assertOnePageSize(entity, 2)
        "modificationDate!:[2022-04-01,2022-04-03]"                                  | (entity, list) -> assertOnePageSize(entity, 1)
        "modificationDate!:2022-04-01"                                               | (entity, list) -> assertOnePageSize(entity, 2)
        "modificationDate>2022-04-01<2022-04-03"                                     | (entity, list) -> assertOnePageSize(entity, 1)
        "modificationDate>:2022-04-01<:2022-04-03"                                   | (entity, list) -> assertOnePageSize(entity, 3)
        "projectPublicId:${ANY_PROJECT_PUBLIC_ID}"                                   | (entity, list) -> assertOnePageSize(entity, 1)
        "projectPublicId!:${ANY_PROJECT_PUBLIC_ID}"                                  | (entity, list) -> assertOnePageSize(entity, list.size() - 1)
    }

    @FunctionalInterface
    interface TaskFilterPredicate {
        boolean test(ResponseEntity<ListResponse<Map<String, String>>> entity, List<TaskBuilder> list)
    }
}
