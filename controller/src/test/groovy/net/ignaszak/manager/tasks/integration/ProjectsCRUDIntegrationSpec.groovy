package net.ignaszak.manager.tasks.integration

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.commons.test.specification.IntegrationSpecification
import net.ignaszak.manager.tasks.integration.builder.ProjectBuilder
import net.ignaszak.manager.tasks.integration.builder.ProjectMemberBuilder
import net.ignaszak.manager.tasks.integration.builder.TaskBuilder
import net.ignaszak.manager.tasks.rest.request.CreateMemberRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateProjectRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import net.ignaszak.manager.tasks.rest.request.UpdateMemberRequest
import org.springframework.http.ResponseEntity
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared

import java.sql.Connection
import java.time.LocalDateTime

import static net.ignaszak.manager.commons.test.TestUtils.assertOnePageSize
import static net.ignaszak.manager.tasks.TestConstants.ANY_OTHER_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_OTHER_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_OTHER_USER_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_TITLE
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_TEXT
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_TITLE
import static net.ignaszak.manager.tasks.TestHelper.assertDate
import static net.ignaszak.manager.tasks.entity.ProjectMemberEntity.UserRole.ADMINISTRATOR
import static net.ignaszak.manager.tasks.entity.ProjectMemberEntity.UserRole.MEMBER
import static net.ignaszak.manager.tasks.integration.DBHelper.NOW
import static net.ignaszak.manager.tasks.integration.DBHelper.count
import static net.ignaszak.manager.tasks.integration.DBHelper.countByPublicId
import static net.ignaszak.manager.tasks.integration.DBHelper.selectByPublicId
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_INVALID_MEMBER_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_INVALID_PUBLIC_ID
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_INVALID_TITLE
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_EXISTS
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_FORBIDDEN
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_MEMBER_NOT_FOUND
import static net.ignaszak.manager.tasks.service.project.error.ProjectError.PRO_NOT_FOUND
import static net.ignaszak.manager.tasks.service.task.error.TaskError.TSK_INVALID_TITLE
import static net.ignaszak.manager.tasks.service.task.error.TaskError.TSK_NOT_ATTACHED_TO_PROJECT
import static net.ignaszak.manager.tasks.service.task.error.TaskError.TSK_NOT_FOUND
import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.FORBIDDEN
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

class ProjectsCRUDIntegrationSpec extends IntegrationSpecification {

    @Shared
    private static final PostgreSQLContainer POSTGRESQL_CONTAINER = new PostgreSQLContainer("postgres:15.0")
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa")

    private static Connection CONNECTION
    private static DBHelper DB_HELPER

    private static final String NEW_PROJECT_TITLE = "new title"

    def cleanup() {
        DB_HELPER.clearProject()
    }

    def cleanupSpec() {
        POSTGRESQL_CONTAINER.stop()
    }

    @DynamicPropertySource
    private static void postgresqlProperties(DynamicPropertyRegistry registry) {
        POSTGRESQL_CONTAINER.start()

        registry.add("spring.datasource.url", POSTGRESQL_CONTAINER::getJdbcUrl)
        registry.add("spring.datasource.username", POSTGRESQL_CONTAINER::getUsername)
        registry.add("spring.datasource.password", POSTGRESQL_CONTAINER::getPassword)

        HikariConfig hikariConfig = new HikariConfig()
        hikariConfig.setJdbcUrl(POSTGRESQL_CONTAINER.getJdbcUrl())
        hikariConfig.setUsername(POSTGRESQL_CONTAINER.getUsername())
        hikariConfig.setPassword(POSTGRESQL_CONTAINER.getPassword())
        CONNECTION = new HikariDataSource(hikariConfig).getConnection()
        DB_HELPER = new DBHelper(CONNECTION)
    }

    @SuppressWarnings("all")
    def "create project" () {
        given:
        def request = new CreateUpdateProjectRequest(ANY_PROJECT_TITLE)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == CREATED
        !responseEntity.body.data.publicId.isBlank()
        responseEntity.body.data.title == ANY_PROJECT_TITLE
        responseEntity.body.data.creationDate != null
        responseEntity.body.data.modificationDate == null

        and:
        def projectStmt = CONNECTION.prepareStatement(countByPublicId("project"))
        projectStmt.setString(1, responseEntity.body.data.publicId)
        def projectResultSet = projectStmt.executeQuery()
        projectResultSet.next()
        projectResultSet.getInt(1) == 1

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_public_id, user_role, is_project_creator FROM project_member WHERE project_id = ?")
        memberStmt.setInt(1, 1)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_public_id") == UserHelper.ANY_PUBLIC_ID
        memberResultSet.getString("user_role") == ADMINISTRATOR.name()
        memberResultSet.getBoolean("is_project_creator")
    }

    def "error while creating project with invalid data"() {
        given:
        def request = new CreateUpdateProjectRequest("")

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_TITLE)
    }

    def "get project by public id"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}") as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.data.publicId == ANY_PROJECT_PUBLIC_ID
        responseEntity.body.data.title == ANY_PROJECT_TITLE
        assertDate(NOW, responseEntity.body.data.creationDate)
        responseEntity.body.data.modificationDate == null
    }

    def "return not found while getting project by public id"() {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "return error while getting project by invalid public id"() {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/invalidPublicId")

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_PUBLIC_ID)
    }

    def "get all projects"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        assertPaging(responseEntity)
        responseEntity.body.list.size() == 1
        responseEntity.body.list[0].publicId == ANY_PROJECT_PUBLIC_ID
        responseEntity.body.list[0].title == ANY_PROJECT_TITLE
        assertDate(NOW, responseEntity.body.list[0].creationDate)
        responseEntity.body.list[0].modificationDate == null
    }

    def "paginate projects"(int page, int elements) {
        setup:
        int counter = 0
        def publicIds = new ArrayList<String>()
        while (counter++ < 46) {
            def publicId = UUID.randomUUID().toString()

            DB_HELPER.insert(
                    ProjectBuilder.new().setPublicId(publicId)
            )

            publicIds.add(publicId)
        }

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects?page=${page}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        pagingAssertion(responseEntity)
            .withNumber(page)
            .withTotalPages(3)
            .withTotalElements(46)
            .isValid()
        responseEntity.body.list.size() == elements
        !responseEntity.body.list[0].publicId.isBlank()
        responseEntity.body.list[0].title == ANY_PROJECT_TITLE
        assertDate(NOW, responseEntity.body.list[0].creationDate)
        responseEntity.body.list[0].modificationDate == null
        int i = 0
        for (; i < elements; i++) {
            responseEntity.body.list[i].publicId == publicIds.get( ((page - 1) * 20) + i )
        }
        i == elements

        where:
        page | elements
        1    | 20
        2    | 20
        3    | 6
    }

    @SuppressWarnings("all")
    def "filter projects" (String filter, ProjectFilterPredicate predicate) {
        setup:
        def projects = List.of(
                ProjectBuilder.new(),
                ProjectBuilder.new().setPublicId(UUID.randomUUID().toString()).setTitle("some title"),
                ProjectBuilder.new().setPublicId(UUID.randomUUID().toString()).setCreationDate(LocalDateTime.of(2022, 04, 01, 0, 1)),
                ProjectBuilder.new().setPublicId(UUID.randomUUID().toString()).setModificationDate(LocalDateTime.of(2022, 04, 01, 0, 1)),
        )
        projects.forEach(DB_HELPER::insert)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects?filter=${filter}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        predicate.test(responseEntity, projects)

        where:
        filter                              | predicate
        "publicId:${ANY_PROJECT_PUBLIC_ID}" | (entity, list) -> assertOnePageSize(entity, 1)
        "title::some"                       | (entity, list) -> assertOnePageSize(entity, 1)
        "creationDate<:2022-04-01"          | (entity, list) -> assertOnePageSize(entity, 1)
        "modificationDate:2022-04-01"       | (entity, list) -> assertOnePageSize(entity, 1)
    }

    def "return empty list of all projects"() {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.list.size() == 0
    }

    def "update project by public id"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        def request = new CreateUpdateProjectRequest(NEW_PROJECT_TITLE)

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.data.publicId == ANY_PROJECT_PUBLIC_ID
        responseEntity.body.data.title == NEW_PROJECT_TITLE
        assertDate(NOW, responseEntity.body.data.creationDate)
        responseEntity.body.data.modificationDate != null
    }

    def "return not found while updating project"() {
        given:
        def request = new CreateUpdateProjectRequest(ANY_PROJECT_TITLE)

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "return bad request while updating project with invalid public id"() {
        given:
        def request = new CreateUpdateProjectRequest(ANY_PROJECT_TITLE)

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/invalidProjectPublicId", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_PUBLIC_ID)
    }

    def "return bad request while updating project with invalid data"() {
        given:
        def request = new CreateUpdateProjectRequest("")

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_TITLE)
    }

    def "successful delete project by public id"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}") as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertNoContent(responseEntity)

        and:
        def stmt = CONNECTION.prepareStatement(countByPublicId("project"))
        stmt.setString(1, ANY_PROJECT_PUBLIC_ID)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getInt(1) == 0
    }

    def "return bad request while deleting project with invalid public id"() {
        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/invalidProjectPublicId") as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_PUBLIC_ID)
    }

    def "return not found while deleting no existing project"() {
        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}") as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "get project members"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                .setUserRole(ADMINISTRATOR)
        )
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
                        .setUserRole(MEMBER)
        )

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members") as ResponseEntity<ListResponse>

        then:
        pagingAssertion(responseEntity).withTotalElements(2).isValid()
        responseEntity.statusCode == OK
        responseEntity.body.list != null
        responseEntity.body.list.size() == 2
    }

    def "paginate project members"(int page, int elements) {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )

        int counter = 0
        def publicIds = new ArrayList<String>()
        while (counter++ < 45) {
            def publicId = UUID.randomUUID().toString()

            DB_HELPER.insert(
                    ProjectMemberBuilder.new()
                    .setUserPublicId(publicId)
                    .setUserRole(MEMBER)
            )

            publicIds.add(publicId)
        }

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members?page=${page}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        pagingAssertion(responseEntity)
                .withNumber(page)
                .withTotalPages(3)
                .withTotalElements(46).isValid()
        responseEntity.statusCode == OK
        responseEntity.body.list != null
        responseEntity.body.list.size() == elements
        int i = 0
        for (; i < elements; i++) {
            def index = ((page - 1) * 20) + i
            if (publicIds.size() < index) {
                responseEntity.body.list[i].publicId == publicIds.get(index)
            }
        }
        i == elements

        where:
        page | elements
        1    | 20
        2    | 20
        3    | 6
    }

    def "return invalid project public id while getting project members"() {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/invalidProjectPublicId/members")

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_PUBLIC_ID)
    }

    def "return not found if project does not exists while getting project members"() {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "return forbidden if user is not a member while getting project members" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                .setUserPublicId(ANY_OTHER_PROJECT_PUBLIC_ID)
        )

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)
    }

    @SuppressWarnings("all")
    def "add member to project by its administrator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                .setUserRole(ADMINISTRATOR)
        )

        def request = new CreateMemberRequest(ANY_OTHER_USER_PUBLIC_ID)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        assertNoContent(responseEntity)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_role, is_project_creator FROM project_member WHERE project_id = ? AND user_public_id = ?")
        memberStmt.setInt(1, 1)
        memberStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_role") == MEMBER.name()
        !memberResultSet.getBoolean("is_project_creator")
    }

    def "return bad request with invalid request while adding member to project" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())

        def request = new CreateMemberRequest("invalidId")

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_MEMBER_PUBLIC_ID)
    }

    def "return bad request with invalid project public id while adding member to project" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())

        def request = new CreateMemberRequest(ANY_OTHER_USER_PUBLIC_ID)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/invalidProjectPublicId/members", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_INVALID_PUBLIC_ID)
    }

    def "return bad request while adding member to no existing project" () {
        given:
        def request = new CreateMemberRequest(ANY_OTHER_USER_PUBLIC_ID)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members", request)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    @SuppressWarnings("all")
    def "update project member by administrator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                .setUserRole(ADMINISTRATOR)
        )
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )
        def request = new UpdateMemberRequest(ADMINISTRATOR.name())

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}", request)

        then:
        assertNoContent(responseEntity)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_role FROM project_member WHERE project_id = ? AND user_public_id = ?")
        memberStmt.setInt(1, 1)
        memberStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_role") == ADMINISTRATOR.name()
    }

    @SuppressWarnings("all")
    def "update project administrator by other administrator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
                        .setUserRole(ADMINISTRATOR)
        )
        def request = new UpdateMemberRequest(MEMBER.name())

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}", request)

        then:
        assertNoContent(responseEntity)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_role FROM project_member WHERE project_id = ? AND user_public_id = ?")
        memberStmt.setInt(1, 1)
        memberStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_role") == MEMBER.name()
    }

    @SuppressWarnings("all")
    def "return forbidden while updating changing project creator role to member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
                        .setIsProjectCreator(true)
        )
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
                        .setUserRole(ADMINISTRATOR)
        )
        def request = new UpdateMemberRequest(MEMBER.name())

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${UserHelper.ANY_PUBLIC_ID}", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_role FROM project_member WHERE project_id = ? AND user_public_id = ?")
        memberStmt.setInt(1, 1)
        memberStmt.setString(2, UserHelper.ANY_PUBLIC_ID)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_role") == ADMINISTRATOR.name()
    }

    @SuppressWarnings("all")
    def "return forbidden while updating project member by other project member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )
        def request = new UpdateMemberRequest(ADMINISTRATOR.name())

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_role FROM project_member WHERE project_id = ? AND user_public_id = ?")
        memberStmt.setInt(1, 1)
        memberStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_role") == MEMBER.name()
    }

    @SuppressWarnings("all")
    def "return forbidden while updating project member by user not added to the project" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )
        def request = new UpdateMemberRequest(ADMINISTRATOR.name())

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_role FROM project_member WHERE project_id = ? AND user_public_id = ?")
        memberStmt.setInt(1, 1)
        memberStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_role") == MEMBER.name()
    }

    def "return not found while updating member of not existing project" () {
        given:
        def request = new UpdateMemberRequest(ADMINISTRATOR.name())

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}", request)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "return not found while updating no existing project member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )
        def request = new UpdateMemberRequest(ADMINISTRATOR.name())

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}", request)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_MEMBER_NOT_FOUND)
    }

    @SuppressWarnings("all")
    def "delete other project member by project administrator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}")

        then:
        assertNoContent(responseEntity)

        and:
        def projectStmt = CONNECTION.prepareStatement("SELECT count(*) FROM project_member WHERE project_id = ? AND user_public_id = ?")
        projectStmt.setInt(1, 1)
        projectStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def projectResultSet = projectStmt.executeQuery()
        projectResultSet.next()
        projectResultSet.getInt(1) == 0
    }

    @SuppressWarnings("all")
    def "return forbidden while deleting project creator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
                        .setUserRole(ADMINISTRATOR)
                        .setIsProjectCreator(true)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def projectStmt = CONNECTION.prepareStatement("SELECT count(*) FROM project_member WHERE project_id = ? AND user_public_id = ?")
        projectStmt.setInt(1, 1)
        projectStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def projectResultSet = projectStmt.executeQuery()
        projectResultSet.next()
        projectResultSet.getInt(1) == 1
    }

    @SuppressWarnings("all")
    def "return forbidden while deleting project member by other member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def projectStmt = CONNECTION.prepareStatement("SELECT count(*) FROM project_member WHERE project_id = ? AND user_public_id = ?")
        projectStmt.setInt(1, 1)
        projectStmt.setString(2, ANY_OTHER_USER_PUBLIC_ID)
        def projectResultSet = projectStmt.executeQuery()
        projectResultSet.next()
        projectResultSet.getInt(1) == 1
    }

    def "return forbidden while delete project member by other project member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)
    }

    def "return forbidden while deleting project member by user not added to the project" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)
    }

    def "return not found while deleting member of not existing project" () {
        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "return not found while deleting no existing project member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/${ANY_OTHER_USER_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_MEMBER_NOT_FOUND)
    }

    @SuppressWarnings("all")
    def "join logged user to project as member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/join", null)

        then:
        assertNoContent(responseEntity)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT user_public_id, user_role, is_project_creator FROM project_member WHERE project_id = ?")
        memberStmt.setInt(1, 1)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getString("user_public_id") == UserHelper.ANY_PUBLIC_ID
        memberResultSet.getString("user_role") == MEMBER.name()
        !memberResultSet.getBoolean("is_project_creator")
    }

    @SuppressWarnings("all")
    def "return bad request while joining project by already added user" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/join", null)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, PRO_MEMBER_EXISTS)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT count(*) FROM project_member WHERE project_id = ?")
        memberStmt.setInt(1, 1)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt(1) == 1
    }

    def "return not found while trying to join not existing project" () {
        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/join", null)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    @SuppressWarnings("all")
    def "leave logged user from project" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/leave")

        then:
        assertNoContent(responseEntity)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT count(*) FROM project_member WHERE project_id = ?")
        memberStmt.setInt(1, 1)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt(1) == 0
    }

    @SuppressWarnings("all")
    def "return forbidden while trying to leave from project by its creator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
                        .setIsProjectCreator(true)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/leave")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def memberStmt = CONNECTION.prepareStatement("SELECT count(*) FROM project_member WHERE project_id = ?")
        memberStmt.setInt(1, 1)
        def memberResultSet = memberStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt(1) == 1
    }

    def "return not found while leaving project by no added user" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/leave")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_MEMBER_NOT_FOUND)
    }

    def "return not found while trying to leave no existing project" () {
        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members/leave")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    @SuppressWarnings("all")
    def "filter project members"(String filter, MemberFilterPredicate predicate) {
        given:
        DB_HELPER.insert(ProjectBuilder.new())
        def members = List.of(
                ProjectMemberBuilder.new(),
                ProjectMemberBuilder.new().setUserPublicId(ANY_OTHER_USER_PUBLIC_ID),
                ProjectMemberBuilder.new().setUserPublicId(UUID.randomUUID().toString()).setUserRole(ADMINISTRATOR)
        )
        members.forEach(DB_HELPER::insert)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/members?filter=${filter}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        predicate.test(responseEntity, members)

        where:
        filter                                 | predicate
        "publicId:${ANY_OTHER_USER_PUBLIC_ID}" | (entity, list) -> assertOnePageSize(entity, 1)
        "role:ADMINISTRATOR"                   | (entity, list) -> assertOnePageSize(entity, 1)
        "role!:MEMBER"                         | (entity, list) -> assertOnePageSize(entity, 1)
        "role!:[MEMBER,ADMINISTRATOR]"         | (entity, list) -> assertOnePageSize(entity, 0)
        "role!:unknown"                        | (entity, list) -> assertOnePageSize(entity, list.size())
        "role:unknown"                         | (entity, list) -> assertOnePageSize(entity, list.size())
        "role!:[MEMBER,unknown]"               | (entity, list) -> assertOnePageSize(entity, 1)
        "role!:[unknown,ADMINISTRATOR]"        | (entity, list) -> assertOnePageSize(entity, list.size() - 1)
    }

    def "create task for existing project when user is its member"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        def request = new CreateUpdateTaskRequest(ANY_TASK_TITLE, ANY_TASK_TEXT)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == CREATED
        !responseEntity.body.data.publicId.isBlank()
        responseEntity.body.data.authorPublicId == UserHelper.ANY_PUBLIC_ID
        responseEntity.body.data.title == ANY_TASK_TITLE
        responseEntity.body.data.text == ANY_TASK_TEXT
        responseEntity.body.data.creationDate != null
        responseEntity.body.data.modificationDate == null

        and:
        def stmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        stmt.setString(1, responseEntity.body.data.publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getInt("project_id") == 1
    }

    def "return not found when project not exists while creating new task" () {
        given:
        def request = new CreateUpdateTaskRequest(ANY_TASK_TITLE, ANY_TASK_TEXT)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks", request)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)

        and:
        def stmt = CONNECTION.prepareStatement(count("task"))
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getInt(1) == 0
    }

    def "return forbidden while creating a new task for project when user is not its member"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        def request = new CreateUpdateTaskRequest(ANY_TASK_TITLE, ANY_TASK_TEXT)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def stmt = CONNECTION.prepareStatement(count("task"))
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getInt(1) == 0
    }

    def "return bad request while creating a new task for project with invalid data"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        def request = new CreateUpdateTaskRequest("", "")

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TSK_INVALID_TITLE)

        and:
        def stmt = CONNECTION.prepareStatement(count("task"))
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getInt(1) == 0
    }

    def "attach task to project when user is task creator and project member and task is not attached to any project"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(TaskBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}", null)

        then:
        assertNoContent(responseEntity)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 1
    }

    def "return forbidden while attaching task to project by task creator but not project member"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(TaskBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}", null)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 0
    }

    def "return forbidden while attaching task by project member when user is not task creator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                TaskBuilder.new()
                        .setAuthorPublicId(ANY_OTHER_USER_PUBLIC_ID)
        )

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}", null)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 0
    }

    def "return not found when task not exists while attaching it to project" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}", null)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, TSK_NOT_FOUND)
    }

    def "return not found while attaching task to no existing project" () {
        when:
        def responseEntity = restTemplateAuthFacade.post("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}", null)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "detach task from project by task creator who is project member" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                TaskBuilder.new()
                        .setProjectId(1)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertNoContent(responseEntity)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 0
    }

    def "detach task from project by project administrator" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectMemberBuilder.new()
                        .setUserRole(ADMINISTRATOR)
        )
        DB_HELPER.insert(
                TaskBuilder.new()
                        .setAuthorPublicId(ANY_OTHER_USER_PUBLIC_ID)
                        .setProjectId(1)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertNoContent(responseEntity)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 0
    }

    def "return forbidden while detaching task from project by other member"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                TaskBuilder.new()
                        .setAuthorPublicId(ANY_OTHER_USER_PUBLIC_ID)
                        .setProjectId(1)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 1
    }

    def "return forbidden while detaching task from project by not project member"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                TaskBuilder.new()
                        .setProjectId(1)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, PRO_MEMBER_FORBIDDEN)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 1
    }

    def "return bad request while detaching task from project when task is not attached to any project" () {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(TaskBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TSK_NOT_ATTACHED_TO_PROJECT)
    }

    def "return bad request while detaching task from other project"() {
        setup:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(
                ProjectBuilder.new()
                        .setPublicId(ANY_OTHER_PROJECT_PUBLIC_ID)
        )
        DB_HELPER.insert(ProjectMemberBuilder.new())
        DB_HELPER.insert(
                TaskBuilder.new()
                        .setProjectId(2)
        )

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, TSK_NOT_ATTACHED_TO_PROJECT)

        and:
        def taskStmt = CONNECTION.prepareStatement(selectByPublicId("task", "project_id"))
        taskStmt.setString(1, ANY_TASK_PUBLIC_ID)
        def memberResultSet = taskStmt.executeQuery()
        memberResultSet.next()
        memberResultSet.getInt("project_id") == 2
    }

    def "return not found while detaching task from no existing project"() {
        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    def "return not found while detaching no existing task"() {
        given:
        DB_HELPER.insert(ProjectBuilder.new())

        when:
        def responseEntity = restTemplateAuthFacade.delete("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks/${ANY_TASK_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, TSK_NOT_FOUND)
    }

    def "return project tasks list"() {
        given:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(TaskBuilder.new().setProjectId(1))
        DB_HELPER.insert(TaskBuilder.new().setPublicId(ANY_OTHER_TASK_PUBLIC_ID))

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.list.size() == 1
        responseEntity.body.list[0].publicId == ANY_TASK_PUBLIC_ID
    }

    def "return project tasks list ignoring projectPublicId filter"() {
        given:
        DB_HELPER.insert(ProjectBuilder.new())
        DB_HELPER.insert(ProjectBuilder.new().setPublicId(ANY_OTHER_PROJECT_PUBLIC_ID))
        DB_HELPER.insert(TaskBuilder.new().setProjectId(1))
        DB_HELPER.insert(TaskBuilder.new().setPublicId(ANY_OTHER_TASK_PUBLIC_ID).setProjectId(2))

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks?filter=projectPublicId:${ANY_OTHER_PROJECT_PUBLIC_ID}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.list.size() == 1
        responseEntity.body.list[0].publicId == ANY_TASK_PUBLIC_ID
    }

    def "return not found when project not exists while getting project tasks"() {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/projects/${ANY_PROJECT_PUBLIC_ID}/tasks")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, PRO_NOT_FOUND)
    }

    @FunctionalInterface
    interface ProjectFilterPredicate {
        boolean test(ResponseEntity<ListResponse<Map<String, String>>> entity, List<ProjectBuilder> list)
    }

    @FunctionalInterface
    interface MemberFilterPredicate {
        boolean test(ResponseEntity<ListResponse<Map<String, String>>> entity, List<ProjectMemberBuilder> list)
    }
}
