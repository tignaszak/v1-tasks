package net.ignaszak.manager.tasks.controller

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import net.ignaszak.manager.tasks.facade.ServiceFacade
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType

import java.nio.charset.StandardCharsets

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.TestUtils.assertResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson
import static net.ignaszak.manager.tasks.TestConstants.ANY_TASK_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestHelper.anyCreateUpdateTaskRequest
import static net.ignaszak.manager.tasks.TestHelper.anyPageResponse
import static net.ignaszak.manager.tasks.TestHelper.anyTaskResponse
import static net.ignaszak.manager.tasks.TestHelper.expectTaskResponse
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(TasksController)
class TasksControllerSpec extends ControllerSpecification {

    @SpringBean
    private ServiceFacade serviceFacadeMock = Mock()

    def "create new task"() {
        given:
        1 * serviceFacadeMock.createTask(anyCreateUpdateTaskRequest(), UserHelper.ANY_PUBLIC_ID) >> new DataResponse(anyTaskResponse())

        when:
        def request = post("/api")
                .header(TOKEN_HEADER, anyToken())
                .content(toJson(anyCreateUpdateTaskRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        expectTaskResponse(mvc.perform(request), status().isCreated())
    }

    def "get task by public id"() {
        given:
        1 * serviceFacadeMock.getTaskById(ANY_TASK_PUBLIC_ID, UserHelper.ANY_PUBLIC_ID) >> new DataResponse(anyTaskResponse())

        when:
        def request = get("/api/{publicId}", ANY_TASK_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        expectTaskResponse(mvc.perform(request), status().isOk())
    }

    def "update task by public id"() {
        given:
        1 * serviceFacadeMock.updateTask(ANY_TASK_PUBLIC_ID, anyCreateUpdateTaskRequest(), UserHelper.ANY_PUBLIC_ID) >> new DataResponse(anyTaskResponse())

        when:
        def request = put("/api/{publicId}", ANY_TASK_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())
                .content(toJson(anyCreateUpdateTaskRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        expectTaskResponse(mvc.perform(request), status().isOk())
    }

    def "remove task by public id"() {
        when:
        def request = delete("/api/{publicId}", ANY_TASK_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        mvc.perform(request).andExpect(status().isNoContent())
    }

    def "get tasks" () {
        given:
        1 * serviceFacadeMock.getTasks({ PageRequest pageRequest ->
            pageRequest.page == 1
        }, _) >> new ListResponse(anyPageResponse(), Set.of(anyTaskResponse()))

        when:
        def request = get("/api?page=1").header(TOKEN_HEADER, anyToken())

        then:
        assertResponse(mvc.perform(request), status().isOk(), new ListResponse(anyPageResponse(), Set.of(anyTaskResponse())))
    }
}
