package net.ignaszak.manager.tasks

import net.ignaszak.manager.commons.rest.page.PageResponse
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.restcriteria.paging.to.PageCriteriaTO
import net.ignaszak.manager.commons.restcriteria.paging.to.PageTO
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.tasks.rest.request.CreateMemberRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateProjectRequest
import net.ignaszak.manager.tasks.rest.request.CreateUpdateTaskRequest
import net.ignaszak.manager.tasks.rest.request.UpdateMemberRequest
import net.ignaszak.manager.tasks.rest.response.MemberResponse
import net.ignaszak.manager.tasks.rest.response.ProjectResponse
import net.ignaszak.manager.tasks.rest.response.TaskResponse
import net.ignaszak.manager.tasks.to.project.CreateUpdateProjectTO
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import net.ignaszak.manager.tasks.to.project.ProjectTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO
import net.ignaszak.manager.tasks.to.task.CreateUpdateTaskTO
import net.ignaszak.manager.tasks.to.task.TaskFilterTO
import net.ignaszak.manager.tasks.to.task.TaskTO
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher

import java.time.LocalDateTime

import static TestConstants.ANY_DATE
import static TestConstants.ANY_TASK_PUBLIC_ID
import static TestConstants.ANY_TASK_TEXT
import static TestConstants.ANY_TASK_TITLE
import static net.ignaszak.manager.commons.test.TestUtils.assertResponse
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_TITLE

final class TestHelper {

    static expectTaskResponse(ResultActions resultActions, ResultMatcher status) {
        assertResponse(resultActions, status, new DataResponse(anyTaskTO()))
    }

    static expectProjectResponse(ResultActions resultActions, ResultMatcher status) {
        assertResponse(resultActions, status, new DataResponse(anyProjectTO()))
    }

    static expectMemberResponseSet(ResultActions resultActions, ResultMatcher status) {
        assertResponse(resultActions, status, new ListResponse(anyPageResponse(), anyMemberResponseSet()))
    }

    static CreateUpdateTaskRequest anyCreateUpdateTaskRequest() {
        return new CreateUpdateTaskRequest(ANY_TASK_TITLE, ANY_TASK_TEXT)
    }

    static CreateUpdateTaskTO anyCreateUpdateTaskTO() {
        return new CreateUpdateTaskTO(ANY_TASK_TITLE, ANY_TASK_TEXT)
    }

    static TaskTO anyTaskTO() {
        return new TaskTO(ANY_TASK_PUBLIC_ID, UserHelper.ANY_PUBLIC_ID, ANY_TASK_TITLE, ANY_TASK_TEXT, ANY_DATE, ANY_DATE)
    }

    static TaskResponse anyTaskResponse() {
        return new TaskResponse(ANY_TASK_PUBLIC_ID, UserHelper.ANY_PUBLIC_ID, ANY_TASK_TITLE, ANY_TASK_TEXT, ANY_DATE, ANY_DATE)
    }

    static ProjectTO anyProjectTO() {
        return new ProjectTO(ANY_PROJECT_PUBLIC_ID, ANY_PROJECT_TITLE, ANY_DATE, ANY_DATE)
    }

    static ProjectResponse anyProjectResponse() {
        return new ProjectResponse(ANY_PROJECT_PUBLIC_ID, ANY_PROJECT_TITLE, ANY_DATE, ANY_DATE)
    }

    static Set<MemberTO> anyMemberTOSet() {
        Set.of(anyMemberTO())
    }

    static MemberTO anyMemberTO() {
        new MemberTO(UserHelper.ANY_PUBLIC_ID, MemberTO.Role.ADMINISTRATOR)
    }

    static Set<MemberResponse> anyMemberResponseSet() {
        Set.of(anyMemberResponse())
    }

    static MemberResponse anyMemberResponse() {
        new MemberResponse(UserHelper.ANY_PUBLIC_ID, MemberTO.Role.ADMINISTRATOR.name())
    }

    static CreateMemberRequest anyCreateMemberRequest() {
        new CreateMemberRequest(UserHelper.ANY_PUBLIC_ID)
    }

    static UpdateMemberRequest anyUpdateMemberRequest() {
        new UpdateMemberRequest("MEMBER")
    }

    static PageTO anyPageTO() {
        new PageTO(1, 20, 5, 100)
    }

    static PageResponse anyPageResponse() {
        new PageResponse(1, 20, 5, 100)
    }

    static PageCriteriaTO anyPageCriteriaTO() {
        new PageCriteriaTO(1)
    }

    static CriteriaTO<TaskFilterTO> anyTaskFilterCriteriaTO() {
        new CriteriaTO(anyPageCriteriaTO(), anyTaskFilterTO())
    }

    static TaskFilterTO anyTaskFilterTO() {
        new TaskFilterTO(null, null, null, null, null, null, null)
    }

    static CriteriaTO<ProjectFilterTO> anyProjectFilterCriteriaTO() {
        new CriteriaTO(anyPageCriteriaTO(), anyProjectFilterTO())
    }

    static ProjectFilterTO anyProjectFilterTO() {
        new ProjectFilterTO(null, null, null, null)
    }

    static Set<ProjectResponse> anyProjectResponseSet() {
        Set.of(anyProjectResponse())
    }

    static Set<ProjectTO> anyProjectTOSet() {
        Set.of(anyProjectTO())
    }

    static CreateUpdateProjectRequest anyCreateUpdateProjectRequest() {
        new CreateUpdateProjectRequest(ANY_PROJECT_TITLE)
    }

    static CreateUpdateProjectTO anyCreateUpdateProjectTO() {
        new CreateUpdateProjectTO(ANY_PROJECT_TITLE)
    }

    static Set<TaskTO> anyTaskTOSet() {
        Set.of(anyTaskTO())
    }

    static boolean assertDate(LocalDateTime expected, String actual) {
        expected.toString().split("\\.").first() == actual.split("\\.").first()
    }

    private TestHelper() {}
}
