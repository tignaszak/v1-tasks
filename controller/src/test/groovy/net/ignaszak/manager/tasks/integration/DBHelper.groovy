package net.ignaszak.manager.tasks.integration

import net.ignaszak.manager.tasks.integration.builder.ProjectBuilder
import net.ignaszak.manager.tasks.integration.builder.ProjectMemberBuilder
import net.ignaszak.manager.tasks.integration.builder.TaskBuilder

import java.sql.Connection
import java.sql.Timestamp
import java.time.LocalDateTime

class DBHelper {

    public static final LocalDateTime NOW = LocalDateTime.now()

    private final Connection connection

    DBHelper(Connection connection) {
        this.connection = connection
    }

    @SuppressWarnings("all")
    void clearProject() {
        if (connection != null) {
            connection.prepareStatement("TRUNCATE project RESTART IDENTITY CASCADE").execute()
            connection.prepareStatement("TRUNCATE project_member RESTART IDENTITY CASCADE").execute()
        }
    }

    void insert(Object builder) {
        if (builder instanceof TaskBuilder) {
            insertTask((TaskBuilder) builder)
        } else if (builder instanceof ProjectBuilder) {
            insertProject((ProjectBuilder) builder)
        } else if (builder instanceof ProjectMemberBuilder) {
            insertProjectMember((ProjectMemberBuilder) builder)
        }
    }

    @SuppressWarnings("all")
    private void insertProject(ProjectBuilder projectBuilder) {
        def stmt = connection.prepareStatement("INSERT INTO project " +
                "(public_id, title, creation_date, modification_date) " +
                "VALUES (?, ?, ?, ?)")
        stmt.setString(1, projectBuilder.publicId)
        stmt.setString(2, projectBuilder.title)
        stmt.setTimestamp(3, Timestamp.valueOf(projectBuilder.creationDate))
        stmt.setTimestamp(4, Optional.ofNullable(projectBuilder.modificationDate).map(Timestamp::valueOf).orElse(null))
        stmt.execute()
    }

    @SuppressWarnings("all")
    private void insertProjectMember(ProjectMemberBuilder builder) {
        def stmt = connection.prepareStatement("INSERT INTO project_member " +
                "(user_public_id, user_role, is_project_creator, project_id) " +
                "VALUES (?, ?, ?, ?)")
        stmt.setString(1, builder.userPublicId)
        stmt.setString(2, builder.userRole)
        stmt.setBoolean(3, builder.isProjectCreator)
        stmt.setInt(4, builder.projectId)
        stmt.execute()
    }

    @SuppressWarnings("all")
    private void insertTask(TaskBuilder taskBuilder) {
        def stmt = connection.prepareStatement("INSERT INTO task " +
                "(public_id, author_public_id, title, text, creation_date, modification_date, project_id)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?)")
        stmt.setString(1, taskBuilder.publicId)
        stmt.setString(2, taskBuilder.authorPublicId)
        stmt.setString(3, taskBuilder.title)
        stmt.setString(4, taskBuilder.text)
        stmt.setTimestamp(5, Timestamp.valueOf(taskBuilder.creationDate))
        stmt.setTimestamp(6, Optional.ofNullable(taskBuilder.modificationDate).map(Timestamp::valueOf).orElse(null))
        stmt.setObject(7, taskBuilder.projectId)
        stmt.execute()
    }

    @SuppressWarnings("all")
    void clearTask() {
        if (connection != null) {
            connection.prepareStatement("TRUNCATE task").execute()
        }
    }

    static String countByPublicId(String table) {
        "SELECT count(*) FROM ${table} WHERE public_id = ?"
    }

    static String count(String table) {
        "SELECT count(*) FROM ${table}"
    }

    static String selectByPublicId(String table, String what = "*") {
        "SELECT ${what} FROM ${table} WHERE public_id = ?"
    }
}
