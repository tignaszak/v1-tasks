package net.ignaszak.manager.tasks.controller

import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.test.helper.UserHelper
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import net.ignaszak.manager.tasks.facade.ServiceFacade
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType

import java.nio.charset.StandardCharsets

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.TestUtils.assertNoContentResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestHelper.anyCreateMemberRequest
import static net.ignaszak.manager.tasks.TestHelper.anyMemberResponseSet
import static net.ignaszak.manager.tasks.TestHelper.anyPageResponse
import static net.ignaszak.manager.tasks.TestHelper.anyUpdateMemberRequest
import static net.ignaszak.manager.tasks.TestHelper.expectMemberResponseSet
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(ProjectMembersController)
class ProjectMembersControllerSpec extends ControllerSpecification {

    @SpringBean
    private ServiceFacade serviceFacadeMock = Mock()

    def "get project members"() {
        given:
        1 * serviceFacadeMock.getProjectMembers(
                ANY_PROJECT_PUBLIC_ID,
                {
                    PageRequest pageRequest ->
                    pageRequest.page == 1
                },
                _,
                UserHelper.ANY_PUBLIC_ID
        ) >> new ListResponse(anyPageResponse(), anyMemberResponseSet())

        when:
        def request = get("/api/projects/{publicId}/members?page=1", ANY_PROJECT_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        expectMemberResponseSet(mvc.perform(request), status().isOk())
    }

    def "add new member"() {
        given:
        1 * serviceFacadeMock.addMemberToProject(ANY_PROJECT_PUBLIC_ID, anyCreateMemberRequest(), UserHelper.ANY_PUBLIC_ID)

        when:
        def request = post("/api/projects/{publicId}/members", ANY_PROJECT_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())
                .content(toJson(anyCreateMemberRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    def "update member"() {
        given:
        1 * serviceFacadeMock.updateMember(ANY_PROJECT_PUBLIC_ID, UserHelper.ANY_PUBLIC_ID, anyUpdateMemberRequest(), UserHelper.ANY_PUBLIC_ID)

        when:
        def request = put("/api/projects/{projectPublicId}/members/{memberPublicId}", ANY_PROJECT_PUBLIC_ID, UserHelper.ANY_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())
                .content(toJson(anyUpdateMemberRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    def "delete member" () {
        when:
        def request = delete("/api/projects/{projectPublicId}/members/{memberPublicId}", ANY_PROJECT_PUBLIC_ID, UserHelper.ANY_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    def "join project" () {
        when:
        def request = post("/api/projects/{publicId}/members/join", ANY_PROJECT_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        assertNoContentResponse(mvc.perform(request))
    }

    def "leave project" () {
        when:
        def request = delete("/api/projects/{publicId}/members/leave", ANY_PROJECT_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        assertNoContentResponse(mvc.perform(request))
    }
}
