package net.ignaszak.manager.tasks.integration.builder

import net.ignaszak.manager.tasks.integration.DBHelper

import java.time.LocalDateTime

import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_PUBLIC_ID
import static net.ignaszak.manager.tasks.TestConstants.ANY_PROJECT_TITLE

class ProjectBuilder {

    String publicId = ANY_PROJECT_PUBLIC_ID
    String title = ANY_PROJECT_TITLE
    LocalDateTime creationDate = DBHelper.NOW
    LocalDateTime modificationDate = null


    static ProjectBuilder "new"() {
        new ProjectBuilder()
    }

    private ProjectBuilder() {}

    ProjectBuilder setPublicId(final String publicId) {
        this.publicId = publicId
        this
    }

    ProjectBuilder setTitle(final String title) {
        this.title = title
        this
    }

    ProjectBuilder setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate
        this
    }

    ProjectBuilder setModificationDate(final LocalDateTime modificationDate) {
        this.modificationDate = modificationDate
        this
    }
}
