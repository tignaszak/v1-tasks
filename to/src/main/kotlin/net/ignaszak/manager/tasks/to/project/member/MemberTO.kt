package net.ignaszak.manager.tasks.to.project.member

data class MemberTO(val publicId: String, val role: Role) {
    enum class Role {
        ADMINISTRATOR, MEMBER
    }
}