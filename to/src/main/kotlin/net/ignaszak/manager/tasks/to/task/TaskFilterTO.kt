package net.ignaszak.manager.tasks.to.task

import net.ignaszak.manager.commons.restcriteria.filtering.to.DateOperatorTO
import net.ignaszak.manager.commons.restcriteria.filtering.to.StringOperatorTO

data class TaskFilterTO(
    val publicId: StringOperatorTO?,
    val authorPublicId: StringOperatorTO?,
    val projectPublicId: StringOperatorTO?,
    val title: StringOperatorTO?,
    val text: StringOperatorTO?,
    val creationDate: DateOperatorTO?,
    val modificationDate: DateOperatorTO?
)
