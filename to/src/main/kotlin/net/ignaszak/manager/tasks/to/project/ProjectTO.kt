package net.ignaszak.manager.tasks.to.project

import java.time.LocalDateTime

data class ProjectTO(
    val publicId: String,
    val title: String,
    val creationDate: LocalDateTime,
    val modificationDate: LocalDateTime?
)
