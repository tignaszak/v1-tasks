package net.ignaszak.manager.tasks.to.task

data class CreateUpdateTaskTO(val title: String, val text: String? = null)
