package net.ignaszak.manager.tasks.to.task

import java.time.LocalDateTime

data class TaskTO(
        val publicId: String,
        val authorPublicId: String,
        val title: String? = null,
        val text: String? = null,
        val creationDate: LocalDateTime,
        val modificationDate: LocalDateTime?
)