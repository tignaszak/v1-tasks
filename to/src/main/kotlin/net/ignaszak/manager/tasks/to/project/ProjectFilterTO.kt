package net.ignaszak.manager.tasks.to.project

import net.ignaszak.manager.commons.restcriteria.filtering.to.DateOperatorTO
import net.ignaszak.manager.commons.restcriteria.filtering.to.StringOperatorTO

data class ProjectFilterTO(
    val publicId: StringOperatorTO?,
    val title: StringOperatorTO?,
    val creationDate: DateOperatorTO?,
    val modificationDate: DateOperatorTO?
)
