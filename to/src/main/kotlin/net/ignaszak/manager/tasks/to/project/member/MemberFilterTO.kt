package net.ignaszak.manager.tasks.to.project.member

import net.ignaszak.manager.commons.restcriteria.filtering.to.EnumOperatorTO
import net.ignaszak.manager.commons.restcriteria.filtering.to.StringOperatorTO
import net.ignaszak.manager.tasks.to.project.member.MemberTO.Role

data class MemberFilterTO(
    val publicId: StringOperatorTO?,
    val role: EnumOperatorTO<Role>?
)
