package net.ignaszak.manager.tasks.rest.request

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Task create/update request")
data class CreateUpdateTaskRequest(
    @Schema(description = "Task title", required = true) val title: String,
    @Schema(description = "Task text") val text: String? = null
)
