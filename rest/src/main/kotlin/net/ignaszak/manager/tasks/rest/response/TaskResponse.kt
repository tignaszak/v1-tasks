package net.ignaszak.manager.tasks.rest.response

import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime

@Schema(title = "Task response")
data class TaskResponse(
    @Schema(description = "Task public id") val publicId: String,
    @Schema(description = "Task author public id") val authorPublicId: String,
    @Schema(description = "Task title") val title: String,
    @Schema(description = "Task text") val text: String? = null,
    @Schema(description = "Task creation date") val creationDate: LocalDateTime,
    @Schema(description = "Task modification date") val modificationDate: LocalDateTime?
)
