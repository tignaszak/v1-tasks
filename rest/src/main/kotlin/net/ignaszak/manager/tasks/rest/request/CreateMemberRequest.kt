package net.ignaszak.manager.tasks.rest.request

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Create project member request")
data class CreateMemberRequest(
    @Schema(description = "Member public id", required = true) val publicId: String,

    @Schema(
        description = "Member role",
        allowableValues = ["ADMINISTRATOR", "MEMBER"],
        defaultValue = "MEMBER"
    )
    val role: String
) {
    constructor(publicId: String): this(publicId, "MEMBER")
}