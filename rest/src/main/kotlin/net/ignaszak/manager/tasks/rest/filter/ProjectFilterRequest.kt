package net.ignaszak.manager.tasks.rest.filter

import net.ignaszak.manager.commons.rest.filter.DateOperator
import net.ignaszak.manager.commons.rest.filter.StringOperator

data class ProjectFilterRequest(
    val publicId: StringOperator?,
    val title: StringOperator?,
    val creationDate: DateOperator?,
    val modificationDate: DateOperator?
)
