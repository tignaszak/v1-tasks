package net.ignaszak.manager.tasks.rest.request

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Project create/update request")
data class CreateUpdateProjectRequest(
    @Schema(description = "Project title", required = true) val title: String
)
