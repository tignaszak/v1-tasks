package net.ignaszak.manager.tasks.rest.response

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Member response")
data class MemberResponse(
    @Schema(description = "Member public id") val publicId: String,
    @Schema(description = "Member role") val role: String
)
