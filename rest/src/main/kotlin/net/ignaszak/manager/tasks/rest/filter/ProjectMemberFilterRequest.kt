package net.ignaszak.manager.tasks.rest.filter

import net.ignaszak.manager.commons.rest.filter.StringOperator

data class ProjectMemberFilterRequest(
    val publicId: StringOperator?,
    val role: StringOperator?,
)
