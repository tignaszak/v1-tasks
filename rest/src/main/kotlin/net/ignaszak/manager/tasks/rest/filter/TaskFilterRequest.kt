package net.ignaszak.manager.tasks.rest.filter

import net.ignaszak.manager.commons.rest.filter.DateOperator
import net.ignaszak.manager.commons.rest.filter.StringOperator

data class TaskFilterRequest(
    val publicId: StringOperator?,
    val authorPublicId: StringOperator?,
    val projectPublicId: StringOperator?,
    val title: StringOperator?,
    val text: StringOperator?,
    val creationDate: DateOperator?,
    val modificationDate: DateOperator?
)
