package net.ignaszak.manager.tasks.rest.response

import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime

@Schema(title = "Project response")
data class ProjectResponse(
    @Schema(description = "Project public id") val publicId: String,
    @Schema(description = "Project title") val title: String,
    @Schema(description = "Project creation date") val creationDate: LocalDateTime,
    @Schema(description = "Project modification date") val modificationDate: LocalDateTime?
)
