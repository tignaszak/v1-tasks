package net.ignaszak.manager.tasks.rest.request

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Update project member request")
data class UpdateMemberRequest(
    @Schema(
        description = "Member role",
        allowableValues = ["ADMINISTRATOR", "MEMBER"],
        defaultValue = "MEMBER"
    )
    val role: String
)
