package net.ignaszak.manager.tasks.repository.project

import net.ignaszak.manager.tasks.entity.ProjectEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface ProjectRepository : JpaRepository<ProjectEntity, Long>, ProjectRepositorySupport {
    fun findProjectEntityByPublicId(publicId: String): Optional<ProjectEntity>
}