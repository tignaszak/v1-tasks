package net.ignaszak.manager.tasks.repository.task

import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.tasks.dto.task.TaskDTO
import net.ignaszak.manager.tasks.to.task.TaskFilterTO

interface TaskRepositorySupport {
    fun findTaskDTOSet(criteriaQuery: CriteriaQuery<TaskFilterTO>): Set<TaskDTO>
    fun countTaskDTOSet(criteriaQuery: CriteriaQuery<TaskFilterTO>): Long
}