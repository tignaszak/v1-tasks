package net.ignaszak.manager.tasks.repository.project.member

import com.querydsl.core.QueryResults
import com.querydsl.core.types.Projections
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.enumCriteria
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.publicIdCriteria
import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.tasks.dto.project.member.MemberDTO
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity.UserRole
import net.ignaszak.manager.tasks.entity.QProjectMemberEntity
import net.ignaszak.manager.tasks.to.project.member.MemberFilterTO
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport
import org.springframework.stereotype.Repository
import java.util.Collections.unmodifiableSet
import java.util.LinkedHashSet
import java.util.Optional
import java.util.stream.Collectors

@Repository
open class MemberRepositorySupportImpl : QuerydslRepositorySupport(ProjectMemberEntity::class.java), MemberRepositorySupport {

    companion object {
        private val member: QProjectMemberEntity = QProjectMemberEntity.projectMemberEntity
    }

    override fun findMemberDTOSetByProjectPublicId(publicId: String, criteriaQuery: CriteriaQuery<MemberFilterTO>): Set<MemberDTO> {
        val members = getMemberDTOSetByProjectPublicIdQueryResult(publicId, criteriaQuery)
            .results
            .stream()
            .collect(Collectors.toCollection {
                LinkedHashSet()
            })

        return unmodifiableSet(members)
    }

    override fun countMemberDTOSetByProjectPublicId(publicId: String, criteriaQuery: CriteriaQuery<MemberFilterTO>) =
        getMemberDTOSetByProjectPublicIdQueryResult(publicId, criteriaQuery).total

    private fun getMemberDTOSetByProjectPublicIdQueryResult(publicId: String, criteriaQuery: CriteriaQuery<MemberFilterTO>): QueryResults<MemberDTO> {
        val query = from(member)
            .select(
                Projections.constructor(
                    MemberDTO::class.java,
                    member.userPublicId,
                    member.userRole.stringValue()
                )
            )
            .where(member.project.publicId.eq(publicId))

        publicIdCriteria(query, criteriaQuery.filterTO.publicId, member::userPublicId)
        enumCriteria(query, criteriaQuery.filterTO.role, member::userRole)

        return query
            .orderBy(member.userRole.asc())
            .limit(criteriaQuery.pageQuery.size.toLong())
            .offset(criteriaQuery.pageQuery.offset.toLong())
            .fetchResults()
    }


    override fun findMemberRoleByMemberPublicIdAndProjectPublicId(
        memberPublicId: String,
        projectPublicId: String
    ): Optional<String> {
        val role = from(member)
            .select(member.userRole)
            .where(
                member.project.publicId.eq(projectPublicId)
                    .and(member.userPublicId.eq(memberPublicId))
            )
            .orderBy(member.userRole.asc())
            .fetchOne()

        return Optional.ofNullable(role)
            .map(UserRole::name)
    }
}