package net.ignaszak.manager.tasks.repository.project

import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.tasks.dto.project.ProjectDTO
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import java.util.*

interface ProjectRepositorySupport {
    fun findProjectDTOByPublicId(publicId: String): Optional<ProjectDTO>

    fun findProjectDTOSet(criteriaQuery: CriteriaQuery<ProjectFilterTO>): Set<ProjectDTO>
    fun countProjectDTOSet(criteriaQuery: CriteriaQuery<ProjectFilterTO>): Long
}