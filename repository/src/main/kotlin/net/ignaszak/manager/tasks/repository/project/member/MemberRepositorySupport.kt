package net.ignaszak.manager.tasks.repository.project.member

import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.tasks.dto.project.member.MemberDTO
import net.ignaszak.manager.tasks.to.project.member.MemberFilterTO
import java.util.Optional

interface MemberRepositorySupport {
    fun findMemberDTOSetByProjectPublicId(publicId: String, criteriaQuery: CriteriaQuery<MemberFilterTO>) : Set<MemberDTO>
    fun countMemberDTOSetByProjectPublicId(publicId: String, criteriaQuery: CriteriaQuery<MemberFilterTO>) : Long

    fun findMemberRoleByMemberPublicIdAndProjectPublicId(memberPublicId: String, projectPublicId: String) : Optional<String>
}