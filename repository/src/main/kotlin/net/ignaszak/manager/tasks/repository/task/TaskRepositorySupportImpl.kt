package net.ignaszak.manager.tasks.repository.task

import com.querydsl.core.QueryResults
import com.querydsl.core.types.Projections
import com.querydsl.jpa.JPQLQuery
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.dateCriteria
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.publicIdCriteria
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.textCriteria
import net.ignaszak.manager.commons.restcriteria.filtering.to.StringOperatorTO
import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.tasks.dto.task.TaskDTO
import net.ignaszak.manager.tasks.entity.QProjectEntity
import net.ignaszak.manager.tasks.entity.QTaskEntity
import net.ignaszak.manager.tasks.entity.TaskEntity
import net.ignaszak.manager.tasks.to.task.TaskFilterTO
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport
import org.springframework.stereotype.Repository
import java.util.*
import java.util.Collections.unmodifiableSet
import java.util.stream.Collectors

@Repository
open class TaskRepositorySupportImpl: QuerydslRepositorySupport(TaskEntity::class.java), TaskRepositorySupport {
    companion object {
        private val task: QTaskEntity = QTaskEntity.taskEntity
        private val project: QProjectEntity = QProjectEntity.projectEntity
    }

    override fun findTaskDTOSet(criteriaQuery: CriteriaQuery<TaskFilterTO>): Set<TaskDTO> {
        val taskDTOSet = getTaskDTOQueryResults(criteriaQuery)
            .results
            .stream()
            .collect(Collectors.toCollection {
                LinkedHashSet()
            })

        return unmodifiableSet(taskDTOSet)
    }

    override fun countTaskDTOSet(criteriaQuery: CriteriaQuery<TaskFilterTO>) = getTaskDTOQueryResults(criteriaQuery).total

    private fun getTaskDTOQueryResults(criteriaQuery: CriteriaQuery<TaskFilterTO>) : QueryResults<TaskDTO> {
        val query = from(task)
            .select(
                Projections.constructor(
                    TaskDTO::class.java,
                    task.publicId,
                    task.authorPublicId,
                    task.title,
                    task.text,
                    task.creationDate,
                    task.modificationDate
                )
            )

        publicIdCriteria(query, criteriaQuery.filterTO.publicId, task::publicId)
        publicIdCriteria(query, criteriaQuery.filterTO.authorPublicId, task::authorPublicId)
        projectPublicIdCriteria(query, criteriaQuery.filterTO.projectPublicId)
        textCriteria(query, criteriaQuery.filterTO.title, task::title)
        textCriteria(query, criteriaQuery.filterTO.text, task::text)
        dateCriteria(query, criteriaQuery.filterTO.creationDate, task::creationDate)
        dateCriteria(query, criteriaQuery.filterTO.modificationDate, task::modificationDate)

        return query
            .orderBy(task.creationDate.asc())
            .limit(criteriaQuery.pageQuery.size.toLong())
            .offset(criteriaQuery.pageQuery.offset.toLong())
            .fetchResults()
    }

    private fun projectPublicIdCriteria(query: JPQLQuery<*>, operator: StringOperatorTO?) {
        operator?.let {
            publicIdCriteria(query.leftJoin(task.project, project), operator, project::publicId)
        }
    }
}