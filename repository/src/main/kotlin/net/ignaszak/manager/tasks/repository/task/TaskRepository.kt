package net.ignaszak.manager.tasks.repository.task

import net.ignaszak.manager.tasks.entity.TaskEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TaskRepository : JpaRepository<TaskEntity, Long>, TaskRepositorySupport {
    fun findTaskEntityByPublicId(id: String): Optional<TaskEntity>
}