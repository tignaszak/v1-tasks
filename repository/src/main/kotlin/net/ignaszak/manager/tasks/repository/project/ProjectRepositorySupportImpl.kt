package net.ignaszak.manager.tasks.repository.project

import com.querydsl.core.QueryResults
import com.querydsl.core.types.Projections
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.dateCriteria
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.publicIdCriteria
import net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.textCriteria
import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.tasks.dto.project.ProjectDTO
import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.QProjectEntity
import net.ignaszak.manager.tasks.to.project.ProjectFilterTO
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport
import org.springframework.stereotype.Repository
import java.util.*
import java.util.Collections.unmodifiableSet
import java.util.stream.Collectors

@Repository
open class ProjectRepositorySupportImpl: QuerydslRepositorySupport(ProjectEntity::class.java), ProjectRepositorySupport {

    companion object {
        private val project: QProjectEntity = QProjectEntity.projectEntity
    }

    override fun findProjectDTOByPublicId(publicId: String): Optional<ProjectDTO> {
        val projectDTO = from(project)
            .select(
                Projections.constructor(
                    ProjectDTO::class.java,
                    project.publicId,
                    project.title,
                    project.creationDate,
                    project.modificationDate
                )
            )
            .where(project.publicId.eq(publicId))
            .fetchOne()

        return Optional.ofNullable(projectDTO)
    }

    override fun findProjectDTOSet(criteriaQuery: CriteriaQuery<ProjectFilterTO>): Set<ProjectDTO> {
        val projectDTOSet = getProjectDTOSetQueryResults(criteriaQuery)
            .results
            .stream()
            .collect(Collectors.toCollection {
                LinkedHashSet()
            })

        return unmodifiableSet(projectDTOSet)
    }

    override fun countProjectDTOSet(criteriaQuery: CriteriaQuery<ProjectFilterTO>) = getProjectDTOSetQueryResults(criteriaQuery).total

    private fun getProjectDTOSetQueryResults(criteriaQuery: CriteriaQuery<ProjectFilterTO>) : QueryResults<ProjectDTO> {
        val query = from(project)
            .select(
                Projections.constructor(
                    ProjectDTO::class.java,
                    project.publicId,
                    project.title,
                    project.creationDate,
                    project.modificationDate
                )
            )

        publicIdCriteria(query, criteriaQuery.filterTO.publicId, project::publicId)
        textCriteria(query, criteriaQuery.filterTO.title, project::title)
        dateCriteria(query, criteriaQuery.filterTO.creationDate, project::creationDate)
        dateCriteria(query, criteriaQuery.filterTO.modificationDate, project::modificationDate)

        return query
            .orderBy(project.creationDate.asc())
            .limit(criteriaQuery.pageQuery.size.toLong())
            .offset(criteriaQuery.pageQuery.offset.toLong())
            .fetchResults()
    }
}