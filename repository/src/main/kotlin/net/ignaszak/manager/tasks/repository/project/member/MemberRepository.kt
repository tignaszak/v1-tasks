package net.ignaszak.manager.tasks.repository.project.member

import net.ignaszak.manager.tasks.entity.ProjectEntity
import net.ignaszak.manager.tasks.entity.ProjectMemberEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface MemberRepository: JpaRepository<ProjectMemberEntity, Long>, MemberRepositorySupport {
    fun existsProjectMemberEntityByUserPublicIdAndProject(userPublicId: String, projectEntity: ProjectEntity): Boolean
    fun findProjectMemberEntityByUserPublicIdAndProject(userPublicId: String, projectEntity: ProjectEntity): Optional<ProjectMemberEntity>
}