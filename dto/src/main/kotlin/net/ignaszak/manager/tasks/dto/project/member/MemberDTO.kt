package net.ignaszak.manager.tasks.dto.project.member

data class MemberDTO(val publicId: String, val role: String)