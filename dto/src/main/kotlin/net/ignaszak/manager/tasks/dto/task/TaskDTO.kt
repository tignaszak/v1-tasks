package net.ignaszak.manager.tasks.dto.task

import java.time.LocalDateTime

data class TaskDTO(
    val publicId: String,
    val authorPublicId: String,
    val title: String? = null,
    val text: String? = null,
    val creationDate: LocalDateTime,
    val modificationDate: LocalDateTime?
)
