package net.ignaszak.manager.tasks.dto.project

import java.time.LocalDateTime

data class ProjectDTO(
    val publicId: String,
    val title: String,
    val creationDate: LocalDateTime,
    var modificationDate: LocalDateTime? = null,
)
