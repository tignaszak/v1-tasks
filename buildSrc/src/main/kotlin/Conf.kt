import org.gradle.api.Project
import org.gradle.kotlin.dsl.*

object Conf {

    val LOCAL_MAVEN_REPOSITORY = "maven-repository/"

    const val GROUP = "net.ignaszak"
    const val VERSION = "1.0.0-SNAPSHOT"

    object Versions {
        const val MANAGER_COMMONS = "master-SNAPSHOT"

        const val KOTLIN = "1.7.20"
        const val JAVA = "18"

        const val SPRING_BOOT = "2.7.5"
        const val SPRING_CLOUD = "3.1.4"
        const val SPRING_EUREKA = "3.1.4"
        const val SPRING_FRAMEWORK = "5.3.23"
        const val SPRING_SECURITY = "5.7.5"

        const val DEPENDENCY_MANAGEMENT = "1.1.0"
        const val GROOVY = "4.0.6"
        const val GSON = "2.10"
        const val HIKARI = "5.0.1"
        const val JACKSON_KOTLIN = "2.13.4"
        const val LIQUIBASE = "4.17.2"
        const val MAPSTRUCT = "1.5.3.Final"
        const val OPENAPI = "1.6.12"
        const val POSTGRESQL = "42.5.0"
        const val QUERYDSL = "5.0.0"
        const val SNAKEYAML = "1.33"
        const val SPOCK = "2.3-groovy-4.0"
        const val SWAGGER_ANNOTATIONS = "2.2.6"
        const val TESTCONTAINERS = "1.17.5"
    }

    object Deps {
        // Kotlin
        const val KOTLIN_REFLECT = "org.jetbrains.kotlin:kotlin-reflect"
        const val KOTLIN_STDLIN_JDK8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
        const val STDLIB = "stdlib"
        // Spock
        const val GROOVY = "org.apache.groovy:groovy:${Versions.GROOVY}"
        const val SPOCK_CORE = "org.spockframework:spock-core:${Versions.SPOCK}"
        const val SPOCK_SPRING = "org.spockframework:spock-spring:${Versions.SPOCK}"
        // Spring
        const val SPRING_BOOT_STARTER_AMQP = "org.springframework.boot:spring-boot-starter-amqp:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_DATA_JPA = "org.springframework.boot:spring-boot-starter-data-jpa:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_SECURITY = "org.springframework.boot:spring-boot-starter-security:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_TEST = "org.springframework.boot:spring-boot-starter-test:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_WEB = "org.springframework.boot:spring-boot-starter-web:${Versions.SPRING_BOOT}"
        const val SPRING_CLOUD_CONFIG_BOOTSTRAP = "org.springframework.cloud:spring-cloud-starter-bootstrap:${Versions.SPRING_CLOUD}"
        const val SPRING_CLOUD_STARTER_CONFIG = "org.springframework.cloud:spring-cloud-starter-config:${Versions.SPRING_CLOUD}"
        const val SPRING_CLOUD_STARTER_NETFLIX_EUREKA_CLIENT = "org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:${Versions.SPRING_EUREKA}"
        const val SPRING_CONTEXT = "org.springframework:spring-context:${Versions.SPRING_FRAMEWORK}"
        const val SPRING_SECURITY_TEST = "org.springframework.security:spring-security-test:${Versions.SPRING_SECURITY}"
        const val SPRING_WEB = "org.springframework:spring-web:${Versions.SPRING_FRAMEWORK}"
        // Commons
        const val MANAGER_COMMONS_CONF = "net.ignaszak:manager-commons-conf:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_ERROR = "net.ignaszak:manager-commons-error:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_JWT = "net.ignaszak:manager-commons-jwt:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_OPENAPI = "net.ignaszak:manager-commons-openapi:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_QUERYDSL = "net.ignaszak:manager-commons-querydsl:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_REST = "net.ignaszak:manager-commons-rest:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_REST_CRITERIA = "net.ignaszak:manager-commons-rest-criteria:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_SPRING = "net.ignaszak:manager-commons-spring:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_TEST = "net.ignaszak:manager-commons-test:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_USER = "net.ignaszak:manager-commons-user:${Versions.MANAGER_COMMONS}"
        const val MANAGER_COMMONS_UTILS = "net.ignaszak:manager-commons-utils:${Versions.MANAGER_COMMONS}"
        // Others
        const val GSON = "com.google.code.gson:gson:${Versions.GSON}"
        const val HIKARI = "com.zaxxer:HikariCP:${Versions.HIKARI}"
        const val JACKSON = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.JACKSON_KOTLIN}"
        const val KOTLIN_NOARG = "org.jetbrains.kotlin:kotlin-noarg:${Versions.KOTLIN}"
        const val LIQUIBASE = "org.liquibase:liquibase-core:${Versions.LIQUIBASE}"
        const val MAPSTRUCT = "org.mapstruct:mapstruct:${Versions.MAPSTRUCT}"
        const val MAPSTRUCT_PROCESSOR = "org.mapstruct:mapstruct-processor:${Versions.MAPSTRUCT}"
        const val OPENAPI = "org.springdoc:springdoc-openapi-webmvc-core:${Versions.OPENAPI}"
        const val OPENAPI_KOTLIN = "org.springdoc:springdoc-openapi-kotlin:${Versions.OPENAPI}"
        const val POSTGRESQL = "org.postgresql:postgresql:${Versions.POSTGRESQL}"
        const val QUERYDSL_APT = "com.querydsl:querydsl-apt:${Versions.QUERYDSL}:jpa"
        const val QUERYDSL_JPA = "com.querydsl:querydsl-jpa:${Versions.QUERYDSL}"
        const val SNAKEYAML = "org.yaml:snakeyaml:${Versions.SNAKEYAML}"
        const val SWAGGER_ANNOTATIONS = "io.swagger.core.v3:swagger-annotations:${Versions.SWAGGER_ANNOTATIONS}"
        const val TESTCONTAINERS_POSTGRESQL = "org.testcontainers:postgresql:${Versions.TESTCONTAINERS}"
    }
}

fun Project.allProjectDeps(){
    dependencies {
        "implementation" (Conf.Deps.KOTLIN_REFLECT)
        "implementation" (Conf.Deps.KOTLIN_STDLIN_JDK8)
        "implementation" (Conf.Deps.MAPSTRUCT)
        "implementation" (Conf.Deps.SNAKEYAML)
        "implementation" (kotlin(Conf.Deps.STDLIB))
        "kapt" (Conf.Deps.MAPSTRUCT_PROCESSOR)
        "testImplementation" (Conf.Deps.SPOCK_CORE)
        "testImplementation" (Conf.Deps.GROOVY)
    }
}